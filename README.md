frontend PHP & backend TypeScript

1. Make the directory '/' as 'http://localhost:{PORT}/' (nested paths aren't alowed)

2. Open backend/App.php and change new PDO('mysql:host=127.0.0.1;dbname=phpdb', 'root', '');

To compile client:

1. Open terminal and 'cd frontend'

2. Run 'npm run watch' or 'npm run compile'

To create a DB structure:

-- Create syntax for TABLE 'comment'
CREATE TABLE `comment` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `userId` int(11) unsigned NOT NULL,
  `date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `message` varchar(1000) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `userId` (`userId`),
  KEY `date` (`date`),
  CONSTRAINT `userId` FOREIGN KEY (`userId`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=latin1;

-- Create syntax for TABLE 'session'
CREATE TABLE `session` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `userId` int(11) unsigned NOT NULL,
  `first` varchar(20) NOT NULL DEFAULT '',
  `second` varchar(20) NOT NULL DEFAULT '',
  `date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `user` (`userId`),
  KEY `date` (`date`),
  KEY `first` (`first`),
  KEY `second` (`second`),
  CONSTRAINT `user` FOREIGN KEY (`userId`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=latin1;

-- Create syntax for TABLE 'spamComment'
CREATE TABLE `spamComment` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `commentId` int(11) unsigned NOT NULL,
  `spam` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `words` varchar(2000) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `commentId` (`commentId`),
  KEY `spam` (`spam`),
  CONSTRAINT `commentId` FOREIGN KEY (`commentId`) REFERENCES `comment` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=124 DEFAULT CHARSET=latin1;

-- Create syntax for TABLE 'spamWord'
CREATE TABLE `spamWord` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `word` varchar(10) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `word` (`word`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

-- Create syntax for TABLE 'user'
CREATE TABLE `user` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(100) NOT NULL DEFAULT '',
  `password` varchar(100) NOT NULL DEFAULT '',
  `date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  KEY `password` (`password`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;