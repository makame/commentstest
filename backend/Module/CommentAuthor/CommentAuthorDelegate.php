<?php namespace Module\CommentAuthor;

use Core\Application;
use Module\Comment;
use Module\Security;

class CommentAuthorDelegate
{
	// Get an comment by id field
	public static function beforeGet($query, $join = [])
	{
		$userTable = Security\Model\User::$table;
		$commentTable = Comment\Model\Comment::$table;

		$query
		->select(["{$userTable}.username"])
		->join("LEFT JOIN {$userTable} ON {$userTable}.id = {$commentTable}.userId");

		return $query;
	}

	// Get an comment by id field
	public static function afterGet($comment, $join = [])
	{
		if ($comment->userId != null && $comment->username != null) {
			$comment->user = [
				"id" => $comment->userId,
				"username" => $comment->username
			];
		}
		
		return $comment;
	}

	// Get an comment by id field
	public static function beforeGetList($query, $join = [])
	{
		$userTable = Security\Model\User::$table;
		$commentTable = Comment\Model\Comment::$table;

		$query
		->select(["{$userTable}.username"])
		->join("LEFT JOIN {$userTable} ON {$userTable}.id = {$commentTable}.userId");

		return $query;
	}

	// Get an comment by id field
	public static function afterGetList($comment, $join = [])
	{
		if (in_array('user', $join)) {
			$comment->user = Security\Model\User::get($comment->userId);
		}
		
		return $comment;
	}
}
?>