<?php namespace Module\Security;

use Core\Application;

// Session can provide security access to the system
class Session
{
	// Get session from cookie [DON'T TRUST IT]
    private static function get()
    {
		if (isset($_COOKIE['session']))
		{
			$cookie = unserialize($_COOKIE['session']);
			$session = new Model\Session();
			$session->userId = $cookie["userId"];
			$session->first = $cookie["first"];
			$session->second = $cookie["second"];
			$session->date = $cookie["date"];
			return $session;
		}

		return null;
    }

	// Set session into cookie [TRUST IT]
    private static function set(Model\Session $session)
    {
		$cookie = array();
		$cookie["userId"] = $session->userId;
		$cookie["first"] = $session->first;
		$cookie["second"] = $session->second;
		$cookie["date"] = $session->date;
		setcookie('session', serialize($cookie), time() + 172800, Application::$config->cookie);
    }

	// Set session into cookie -> Login action [TRUST IT]
    public static function create(int $userId)
    {
		$session = Model\Session::create($userId);
		Session::set($session);
    }
	
	// Delete session from cookie & delete from db -> Logout action [TRUST IT]
	public static function delete()
	{
		// Get a session from cookie [DON'T TRUST IT]
		if ($sessionCookie = Session::get())
		{
			// Check the session from cookie [DON'T TRUST IT]
			if ($session = $sessionCookie->check())
			{
				// Delete the session from DB [TRUST IT]
				$session->delete();
			}

			// Delete the session from cookie [TRUST IT]
			setcookie('session', $_COOKIE['session'], time() - 172800, Application::$config->cookie);
		}
		else
		{
			Application::$answer->Error("The session doesn't exist");
		}
	}

	// Check a current session -> Template check in Actions [TRUST IT]
	public static function check(): Model\Session
	{
		// Get a session from cookie [DON'T TRUST IT]
		$sessionCookie = Session::get();

		// Check the session from cookie [DON'T TRUST IT]
		if (!$sessionCookie)
		{
			Application::$answer->Error('No session', 401);
		}
		
		// Get a session from DB [TRUST IT]
		$session = $sessionCookie->check();

		// Check the session from DB [TRUST IT]
		if (!$session)
		{
			Application::$answer->Error('Wrong session', 401);
		}

		// Update cookie session from a DB session [TRUST IT]
		Session::set($session);
		
		return $session;
	}
}
?>