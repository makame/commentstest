<?php namespace Module\Security\Model;

use Core\Application;
use ORM\Select;
use ORM\Update;
use ORM\Insert;
use ORM\Delete;

// No ORM - only PDO
class User
{
	// Id field
	public $id;
	// Username field
	public $username;
	
	// A table name in DB
	public static $table = "user";

	// Create an user by username, password fields
	public static function create(string $username, string $password, array $join = [])
	{
        $table = User::$table;

        $query = new Insert();

        $query
            ->table($table)
            ->value([
                'username' => $username,
                'password' => $password
            ]);

        $id = $query->insert();

        if ($id >= 0) {
            $user = User::get($id, $join);

            return $user;
        }
	}

	// Get an user by id field
	public static function get(int $id, array $join = [])
	{
        $table = User::$table;

        $query = new Select();

        $query
            ->table($table)
            ->asClass('Module\\Security\\Model\\User')
            ->select(["{$table}.id", "{$table}.username"])
            ->where("id = :id")
            ->data([
                'id' => $id
            ]);

        return $query->getOne();
	}

	// Get an user by username, password fields
	public static function getWithUsernamePassword(string $username, string $password, array $join = [], array $order = [])
	{
        $table = User::$table;

        $query = new Select();

        $query
            ->table($table)
            ->asClass('Module\\Security\\Model\\User')
            ->select(["{$table}.id", "{$table}.username"])
            ->where("username = :username AND password = :password")
            ->data([
                'username' => $username,
                'password' => $password,
            ]);

        return $query->getOne();
	}

	// Delete the user
	public function delete(): bool
	{
        $table = User::$table;

        $query = new Delete();

        $query
            ->table($table)
            ->where("{$table}.id = :id")
            ->data([
                'id' => $this->id
            ])
            ->limit(1);

        $user = $query->delete();

        return count($user) > 0;
	}
}
?>