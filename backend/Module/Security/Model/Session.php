<?php namespace Module\Security\Model;

use Core\Application;
use ORM\Select;
use ORM\Update;
use ORM\Insert;
use ORM\Delete;

// No ORM - only PDO
class Session
{
	// Id field
	public $id;
	// User id field
	public $userId;
	// First field
	public $first;
	// Second field
	public $second;
	// Date field
	public $date;
	
	// A table name in DB
	public static $table = "session";

	// Create a session by user id field
	public static function create(int $userId, array $join = [])
    {
        $table = Session::$table;

        $query = new Insert();

        $query
            ->table($table)
            ->value([
                'userId' => $userId,
                'first' => md5(uniqid(rand(), 1)),
                'second' => md5(uniqid(rand(), 1))
            ]);

        $id = $query->insert();

        if ($id >= 0) {
            $session = Session::get($id, $join);

            return $session;
        }
    }

	// Get a session by id field
	public static function get(int $id, array $join = [])
	{
        $table = Session::$table;

        $query = new Select();

        $query
            ->table($table)
            ->asClass('Module\\Security\\Model\\Session')
            ->select(["{$table}.id", "{$table}.userId", "{$table}.first", "{$table}.second", "{$table}.date"])
            ->where("id = :id")
            ->data([
                'id' => $id
            ]);

        return $query->getOne();
	}

	// Get a session which isn't older 2 days later by use id, first, second fields.
	public static function getWithUserFirstSecond(int $userId, string $first, string $second, array $join = [])
	{
        $table = Session::$table;

        $query = new Select();

        $query
            ->table($table)
            ->asClass('Module\\Security\\Model\\Session')
            ->select(["{$table}.id", "{$table}.userId", "{$table}.first", "{$table}.second", "{$table}.date"])
            ->where([
                "userId = :userId",
                "first = :first",
                "second = :second",
                "TIMESTAMPDIFF(DAY, date, NOW()) <= 2",
            ])
            ->data([
                'userId' => $userId,
                'first' => $first,
                'second' => $second,
            ]);

        return $query->getOne();
	}

	// Get the session
	public function check()
	{
		$session = Session::getWithUserFirstSecond($this->userId, $this->first, $this->second);
		
		// If the session exists try update it
		if ($session) {

            $table = Session::$table;

            // If a session date is older 10 minutes -> update second key and date
		    $query = new Update();

            $query
                ->table($table)
                ->value([
                    'second' => md5(uniqid(rand(),1)),
                ])
                ->value('date = NOW()')
                ->where([
                    "id = :id",
                    "TIMESTAMPDIFF(MINUTE, date, NOW()) >= 10",
                ])
                ->data([
                    'id' => $session->id,
                ])
                ->limit(1);

            $query->update();

            $session = Session::get($session->id);
            return $session;
		}
	}

	// Delete the session
	public function delete(): bool
	{
        $table = Session::$table;

        $query = new Delete();

        $query
            ->table($table)
            ->where("{$table}.id = :id")
            ->data([
                'id' => $this->id
            ])
            ->limit(1);

        $session = $query->delete();

        return count($session) > 0;
	}
}
?>