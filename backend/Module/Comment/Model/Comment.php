<?php namespace Module\Comment\Model;

use Core\Application;
use ORM\Select;
use ORM\Insert;
use ORM\Delete;
use ORM\Event;

// No ORM - only PDO
class Comment
{
	// Id field
	public $id;
	// User id field
	public $userId;
	// Date field
	public $date;
	// Message field
	public $message;
	
	// A table name in DB
	public static $table = "comment";
	
	// A subscription id
	public static $event = "comment";
	
	// A subscription id
	public static $order = [
		"id" => "comment.id",
		"userId" => "comment.userId",
		"message" => "comment.message",
		"date" => "comment.date"
	];

	// Create an comment by user id, message, spam fields
	public static function create(string $userId, string $message, array $join = [])
	{
		$table = Comment::$table;

        $query = new Insert();

        $query
            ->table($table)
            ->value([
                'userId' => $userId,
                'message' => $message
            ]);

		Event::emit(Comment::$event, 'beforeInsert', $query, $join);

        $id = $query->insert();

        if ($id >= 0) {
            $comment = Comment::get($id, $join);

			Event::emit(Comment::$event, 'afterInsert', $comment, $join);

            return $comment;
        }
	}

	// Get an comment by id field
	public static function get(int $id, array $join = [])
	{
		$table = Comment::$table;

		$query = new Select();

		$query
		->table($table)
		->select(["{$table}.id", "{$table}.userId", "{$table}.date", "{$table}.message"])
		->where("{$table}.id = :id")
		->data([
			'id' => (int) $id
		]);

		Event::emit(Comment::$event, 'beforeGet', $query, $join);

		$comment = $query->getOne();

		Event::emit(Comment::$event, 'afterGet', $comment, $join);

		return $comment;
	}

	// Get a comment's list by spam field ordered by date
	public static function getList(array $join = [], array $order = [], int $limit = 10, int $skip = 0)
	{
		$table = Comment::$table;

		$query = new Select();

		$query
		->table($table)
		->select(["{$table}.id", "{$table}.userId", "{$table}.date", "{$table}.message"])
		->limit($limit)
		->skip($skip)
		->orderBy($order, Comment::$order);

		Event::emit(Comment::$event, 'beforeGetList', $query, $join);

		$comments = $query->getList();

		if ($comments) foreach ($comments as $comment) Event::emit(Comment::$event, 'afterGetList', $comment, $join);

		return $comments;
	}

	// Get a comment's list count by spam field ordered by date
	public static function getListCount()
	{
		$table = Comment::$table;

		$query = new Select();

		$query
		->table($table);

		Event::emit(Comment::$event, 'beforeGetListCount', $query);

		return $query->getListCount();
	}

	// Get a comment's list count by spam field ordered by date
	public static function getNewCount($date)
	{
		$table = Comment::$table;

		$query = new Select();

		$query
		->table($table);

		if ($date) {
			$query
			->where("{$table}.date > :date")
			->data([
				"date" => $date
			]);
		}

		Event::emit(Comment::$event, 'beforeGetListCount', $query);

		return $query->getListCount();
	}

	// Delete the comment
	public static function check($comment)
	{
		Event::emit(Comment::$event, 'check', $comment);

		return $comment;
	}

	// Delete the comment
	public static function delete($comment): bool
	{
		$table = Comment::$table;

        $query = new Delete();

        $query
            ->table($table)
            ->value([
                'id' => $comment->id
            ])
            ->limit(1);

		Event::emit(Comment::$event, 'beforeDelete', $query);

        return $query->delete() > 0;
	}
}
?>