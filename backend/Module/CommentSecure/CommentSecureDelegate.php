<?php namespace Module\CommentSecure;

use Core\Application;
use Module\Comment;

class CommentSecureDelegate
{
	// Get an comment by id field
	public static function beforeGet($query, $join)
	{
		$spamCommentTable = Model\SpamComment::$table;
		$commentTable = Comment\Model\Comment::$table;

		$query
		->select(["{$spamCommentTable}.spam", "{$spamCommentTable}.words"])
		->join("LEFT JOIN {$spamCommentTable} ON {$spamCommentTable}.commentId = {$commentTable}.id");

		return $query;
	}

	// Get an comment by id field
	public static function afterGet($comment, $join)
	{
		if ($comment->words) $comment->words = json_decode($comment->words);

		return $comment;
	}

	// Get an comment by id field
	public static function afterInsert($comment, $join)
	{
		Comment\Model\Comment::check($comment);

		return $comment;
	}

	// Get an comment by id field
	public static function beforeGetList($query, $join)
	{
		$spamCommentTable = Model\SpamComment::$table;
		$commentTable = Comment\Model\Comment::$table;

		$query
		->select(["{$spamCommentTable}.spam", "{$spamCommentTable}.words"])
		->join("LEFT JOIN {$spamCommentTable} ON {$spamCommentTable}.commentId = {$commentTable}.id");

		if (Application::$question->spam(null) === true) {
			$query->where("{$spamCommentTable}.spam = 1");
		}
		else if (Application::$question->spam === false) {
			$query->where("{$spamCommentTable}.spam = 0");
		}
		else if (Application::$question->spam === 'unver') {
			$query->where("{$spamCommentTable}.spam is NULL");
		}

		return $query;
	}

	// Get an comment by id field
	public static function afterGetList($comment, $join)
	{
		if ($comment->words) $comment->words = json_decode($comment->words);

		return $comment;
	}

	// Get an comment by id field
	public static function beforeGetListCount($query)
	{
		$spamCommentTable = Model\SpamComment::$table;
		$commentTable = Comment\Model\Comment::$table;

		$query
		->select(["{$spamCommentTable}.spam"])
		->join("LEFT JOIN {$spamCommentTable} ON {$spamCommentTable}.commentId = {$commentTable}.id");

		if (Application::$question->spam(null) === true) {
			$query->where("{$spamCommentTable}.spam = 1");
		}
		else if (Application::$question->spam === false) {
			$query->where("{$spamCommentTable}.spam = 0");
		}
		else if (Application::$question->spam === 'unver') {
			$query->where("{$spamCommentTable}.spam is NULL");
		}

		return $query;
	}

	// Get an comment by id field
	public static function check($comment)
	{
		$spamWords = Model\SpamWord::getListInMessage($comment->message);

		$getWord = function($word)
		{
			return $word->word;
		};
		
		if (count($spamWords)) {
			$spamWords = array_map($getWord, $spamWords);
		}
		else {
			$spamWords = [];
		}

		Model\SpamComment::create($comment->id, json_encode($spamWords), count($spamWords) ? 1 : 0);
	}
}
?>