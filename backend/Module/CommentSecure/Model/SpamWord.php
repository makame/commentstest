<?php namespace Module\CommentSecure\Model;

use Core\Application;
use ORM\Select;
use ORM\Update;
use ORM\Insert;
use ORM\Delete;

// No ORM - only PDO
class SpamWord
{
	// Id field
	public $id;
	// Spam word field
	public $word;
	
	// A table name in DB
	public static $table = "spamWord";
	
	// A subscription id
	public static $event = "spamWord";
	
	// A subscription id
	public static $order = [
		"id" => "spamWord.id",
		"word" => "spamWord.word"
	];

	// Create an comment by user id, message, spam fields
	public static function create(string $word, array $join = [])
	{
        $table = SpamWord::$table;

        $query = new Insert();

        $query
            ->table($table)
            ->value([
                'word' => $word
            ]);

        $id = $query->insert();

        if ($id >= 0) {
            $spamWord = SpamWord::get($id, $join);

            return $spamWord;
        }
	}

	// Get an comment by id field
	public static function get(int $id, array $join = [])
	{
		$table = SpamWord::$table;

		$query = new Select();

		$query
		->table($table)
		->asClass('Module\\CommentSecure\\Model\\SpamWord')
		->select(["{$table}.id", "{$table}.word"])
		->where("id = :id")
		->data([
			'id' => $id
		]);

		return $query->getOne();
	}

	// Get an comment by id field
	public static function getByWord(string $word, array $join = [])
	{
		$table = SpamWord::$table;

		$query = new Select();

		$query
		->table($table)
		->asClass('Module\\CommentSecure\\Model\\SpamWord')
		->select(["{$table}.id", "{$table}.word"])
		->where("word = :word")
		->data([
			'word' => $word
		]);

		return $query->getOne();
	}

	// Get a comment's list
	public static function getListInMessage(string $message, array $join = [], array $order = [])
	{
		$table = SpamWord::$table;

		$query = new Select();

		$query
		->table($table)
		->asClass('Module\\CommentSecure\\Model\\SpamWord')
		->select(["{$table}.id", "{$table}.word"])
		->where("REGEXP_REPLACE(:message, '([[:space:]]|[[:punct:]])', ' ') REGEXP concat('([[:blank:][:punct:]]|^)', REGEXP_REPLACE({$table}.word, ' ', '([[:space:]]*|[[:punct:]]*)'), '([[:blank:][:punct:]]|$)')")
		->data([
			'message' => $message
		]);

		return $query->getList();
	}

	// Get a comment's list
	public static function getList(array $join = [], array $order = [], int $limit = 10, int $skip = 0)
	{
		$table = SpamWord::$table;

		$query = new Select();

		$query
		->table($table)
		->asClass('Module\\CommentSecure\\Model\\SpamWord')
		->select(["{$table}.id", "{$table}.word"])
		->limit($limit)
		->skip($skip)
		->orderBy($order, SpamWord::$order);

		return $query->getList();
	}

	// Get a comment's list count by spam field ordered by date
	public static function getListCount()
	{
		$table = SpamWord::$table;

		$query = new Select();

		$query
		->table($table);

		return $query->getListCount();
	}

	// Delete the word
	public function update(): bool
	{
        $table = SpamWord::$table;

        $query = new Update();

        $query
            ->table($table)
            ->value([
                'word' => $this->word
            ])
            ->where("{$table}.id = :id")
            ->data([
                'id' => $this->id
            ])
            ->limit(1);

        $spamWord = $query->update();

        return count($spamWord) > 0;
	}

	// Delete the word
	public function delete(): bool
	{
        $table = SpamWord::$table;

        $query = new Delete();

        $query
            ->table($table)
            ->where("{$table}.id = :id")
            ->data([
                'id' => $this->id
            ])
            ->limit(1);

        $spamWord = $query->delete();

        return $spamWord > 0;
	}
}
?>