<?php namespace Module\CommentSecure\Model;

use Core\Application;
use ORM\Select;
use ORM\Insert;
use ORM\Delete;

// No ORM - only PDO
class SpamComment
{
	// Id field
	public $id;
	// User id field
	public $commentId;
	// Spam field
	public $spam;
	// Spam field
	public $words;
	
	// A table name in DB
	public static $table = "spamComment";

	// Create an comment by user id, message, spam fields
	public static function create(int $commentId, string $words, int $spam = 0, array $join = [])
	{
        $table = SpamComment::$table;

        $query = new Insert();

        $query
            ->table($table)
            ->value([
                'commentId' => $commentId,
            ])
            ->duplicate([
                'spam' => $spam,
                'words' => $words
            ]);

        $id = $query->insert();

        if ($id >= 0) {
            $spamComment = SpamComment::getByCommentId($commentId, $join);

            return $spamComment;
        }
	}

	// Get an comment by id field
	public static function get(int $id, array $join = [])
	{
        $table = SpamComment::$table;

        $query = new Select();

        $query
            ->table($table)
            ->asClass('Module\\CommentSecure\\Model\\SpamComment')
            ->select(["{$table}.id", "{$table}.commentId", "{$table}.spam", "{$table}.words"])
            ->where("{$table}.id = :id")
            ->data([
                'id' => $id
            ]);

        $spamComment = $query->getOne();

        return $spamComment;
	}

	// Get an comment by id field
	public static function getByCommentId(int $commentId, array $join = [])
	{
        $table = SpamComment::$table;

        $query = new Select();

        $query
            ->table($table)
            ->asClass('Module\\CommentSecure\\Model\\SpamComment')
            ->select(["{$table}.id", "{$table}.commentId", "{$table}.spam", "{$table}.words"])
            ->where("{$table}.commentId = :commentId")
            ->data([
                'commentId' => $commentId
            ]);

        $spamComment = $query->getOne();

        return $spamComment;
	}

	// Delete the comment
	public function delete(): bool
	{
        $table = SpamComment::$table;

        $query = new Delete();

        $query
            ->table($table)
            ->value([
                'id' => $this->id
            ])
            ->limit(1);

        return count($query->delete()) > 0;
	}
}
?>