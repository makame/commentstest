<?php namespace ORM;

use Core\Application;
use \PDO;

class Update {
    private $_table;

    private $_class;

    private $_limit;
    private $_skip;

    private $_whereArray = [];

    private $_valueArray = [];
    private $_valueStringArray = [];

    private $_data = [];
    
    public function table($value) {
        $this->_table = $value;

        return $this;
    }
    
    public function limit(int $value) {
        $this->_limit = $value;

        return $this;
    }
    
    public function skip(int $value) {
        $this->_skip = $value;

        return $this;
    }
    
    public function asClass($value) {
        $this->_class = $value;

        return $this;
    }

    public function value($value) {
        if (is_array($value)) {
            $this->_valueArray = array_merge($this->_valueArray, $value);
        }
        else {
            array_push($this->_valueStringArray, $value);
        }

        return $this;
    }

    public function where($value) {
        if (is_array($value)) {
            $this->_whereArray = array_merge($this->_whereArray, $value);
        }
        else {
            array_push($this->_whereArray, $value);
        }

        return $this;
    }
    
    public function data(array $value) {
        $this->_data = array_merge($this->_data, $value);
        
        return $this;
    }

    public function update() {
        $stmt = Application::$db->prepare($this->stringify());

        if ($this->_limit) $stmt->bindValue(':limit', (int)$this->_limit, PDO::PARAM_INT);
        if ($this->_skip) $stmt->bindValue(':skip', (int)$this->_skip, PDO::PARAM_INT);

        foreach ($this->_valueArray as $key => $value) {
            $stmt->bindValue(':STV' . $key, $value);
        }

        foreach ($this->_data as $key => $value) {
            $stmt->bindValue(':' . $key, $value);
        }

		if ($stmt->execute()) {
			$this->_class ? $stmt->setFetchMode(PDO::FETCH_CLASS, $this->_class) : $stmt->setFetchMode(PDO::FETCH_OBJ);
            return $stmt->fetchAll();
		}
		else {
            $error = [
                "message" => $stmt->errorInfo()[2],
                "query" => $this->stringify(),
                "number" => $stmt->errorInfo()[1],
                "code" => $stmt->errorInfo()[0]
            ];
			Application::$answer->Error($error, 500);
		}
    }

    private function stringify(): string {
        if (!$this->_table) {
            Application::$answer->Error("Table name isn't set", 500);
        }

        $mappingValues = function ($key)
        {
            return($key . ' = :STV' . $key);
        };

        $values = implode(', ', array_map($mappingValues, array_keys($this->_valueArray)));

        if ($this->_valueStringArray) $values = $values . ($values ? ', ' : '') . implode(', ', $this->_valueStringArray);

        $result = "UPDATE {$this->_table} SET " . $values;

        if (count($this->_whereArray)) {
            $where = implode(' AND ', $this->_whereArray);
            $result = $result . ' WHERE ' . $where;
        }

        if ($this->_limit) {
            $result = $result . " LIMIT :limit";
        }

        if ($this->_skip) {
            $result = $result . " OFFSET :skip";
        }

        return $result;
    }
}
?>