<?php namespace ORM;

use Core\Application;

class EventPair {
    public $id;
    public $delegate;

    function __construct($id, $delegate) {
        $this->id = $id;
        $this->delegate = $delegate;
    }
}

class Event {
    private static $delegates = [];

    public static function registry($id, $delegate) {
        array_push(Event::$delegates, new EventPair($id, $delegate));
    }

    public static function emit($id, $function, &$value, $additional = null) {
        foreach (Event::$delegates as $eventPair) {
            if ($id == $eventPair->id && method_exists($eventPair->delegate, $function)) $value = $eventPair->delegate->$function($value, $additional);
        }
    }
}
?>