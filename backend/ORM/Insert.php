<?php namespace ORM;

use Core\Application;

class Insert {
    private $_table;

    private $_valueArray = [];

    private $_duplicateArray = [];

    private $_data = [];
    
    public function table($value) {
        $this->_table = $value;

        return $this;
    }
    
    public function asClass($value) {
        $this->_class = $value;

        return $this;
    }
    
    public function value(array $value) {
        $this->_valueArray = array_merge($this->_valueArray, $value);

        return $this;
    }

    public function duplicate(array $value) {
        $this->_duplicateArray = array_merge($this->_duplicateArray, $value);

        return $this;
    }
    
    public function data(array $value) {
        $this->_data = array_merge($this->_data, $value);
        
        return $this;
    }

    public function insert(): ?int {
        $stmt = Application::$db->prepare($this->stringify());

        foreach ($this->_duplicateArray as $key => $value) {
            $stmt->bindValue(':DKL' . $key, $value);
        }

        foreach ($this->_valueArray as $key => $value) {
            $stmt->bindValue(':VL' . $key, $value);
        }

        foreach ($this->_data as $key => $value) {
            $stmt->bindValue(':' . $key, $value);
        }

		if ($stmt->execute()) {
            return Application::$db->lastInsertId();
		}
		else {
            $error = [
                "message" => $stmt->errorInfo()[2],
                "query" => $this->stringify(),
                "number" => $stmt->errorInfo()[1],
                "code" => $stmt->errorInfo()[0]
            ];
			Application::$answer->Error($error, 500);
		}
    }

    private function stringify(): string {
        if (!$this->_table) {
            Application::$answer->Error("Table name isn't set", 500);
        }

        $mappingValue = function ($key)
        {
            return(':VL' . $key);
        };

        $insert = implode(',', array_keys($this->_valueArray));
        $insertValues = implode(',', array_map($mappingValue, array_keys($this->_valueArray)));

        $result = "INSERT INTO {$this->_table} ({$insert}) VALUES ({$insertValues})";

        if (count($this->_duplicateArray)) {
            $mappingDuplicate = function ($key)
            {
                return($key . ' = :DKL' . $key);
            };

            $duplicate = implode(', ', array_map($mappingDuplicate, array_keys($this->_duplicateArray)));

            $result = $result . ' ON DUPLICATE KEY UPDATE ' . $duplicate;
        }

        return $result;
    }
}
?>