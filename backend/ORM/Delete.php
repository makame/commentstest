<?php namespace ORM;

use Core\Application;
use \PDO;

class Delete {
    private $_table;

    private $_limit;

    private $_whereArray = [];

    private $_data = [];
    
    public function table($value) {
        $this->_table = $value;

        return $this;
    }

    public function limit(int $value) {
        $this->_limit = $value;

        return $this;
    }

    public function where($value) {
        if (is_array($value)) {
            $this->_whereArray = array_merge($this->_whereArray, $value);
        }
        else {
            array_push($this->_whereArray, $value);
        }

        return $this;
    }

    public function data(array $value) {
        $this->_data = array_merge($this->_data, $value);
        
        return $this;
    }

    public function delete(): ?int
    {
        $stmt = Application::$db->prepare($this->stringify());

        if ($this->_limit) $stmt->bindValue(':limit', (int)$this->_limit, PDO::PARAM_INT);
        
        foreach ($this->_data as $key => $value) {
            $stmt->bindValue(':' . $key, $value);
        }

        if ($stmt->execute()) {
            return $stmt->rowCount();
        } else {
            $error = [
                "message" => $stmt->errorInfo()[2],
                "query" => $this->stringify(),
                "number" => $stmt->errorInfo()[1],
                "code" => $stmt->errorInfo()[0]
            ];
			Application::$answer->Error($error, 500);
        }
    }

    private function stringify(): string {
        if (!$this->_table) {
            Application::$answer->Error("Table name isn't set", 500);
        }

        $result = "DELETE FROM {$this->_table}";

        if (count($this->_whereArray)) {
            $where = implode(' AND ', $this->_whereArray);
            $result = $result . ' WHERE ' . $where;
        }

        if ($this->_limit) {
            $result = $result . " LIMIT :limit";
        }

        return $result;
    }
}
?>