<?php namespace ORM;

use Core\Application;
use \PDO;

class Select {
    private $_table;

    private $_class;

    private $_limit;
    private $_skip;

    private $_selectArray = [];
    private $_whereArray = [];
    private $_orderByArray = [];
    private $_orderByMap = array();

    private $_joinArray = [];

    private $_data = [];
    
    public function table($value) {
        $this->_table = $value;

        return $this;
    }
    
    public function limit(int $value) {
        $this->_limit = $value;

        return $this;
    }
    
    public function skip(int $value) {
        $this->_skip = $value;

        return $this;
    }
    
    public function asClass($value) {
        $this->_class = $value;

        return $this;
    }

    public function select($value) {
        if (is_array($value)) {
            $this->_selectArray = array_merge($this->_selectArray, $value);
        }
        else {
            array_push($this->_selectArray, $value);
        }

        return $this;
    }
    
    public function where($value) {
        if (is_array($value)) {
            $this->_whereArray = array_merge($this->_whereArray, $value);
        }
        else {
            array_push($this->_whereArray, $value);
        }

        return $this;
    }
    
    public function orderBy($value, $map) {
        if (is_array($value)) {
            $this->_orderByArray = array_merge($this->_orderByArray, $value);
            $this->_orderByMap = array_merge($this->_orderByMap, $map);
        }
        else {
            array_push($this->_orderByArray, $value);
            $this->_orderByMap = array_merge($this->_orderByMap, $map);
        }

        return $this;
    }
    
    public function join($value) {
        array_push($this->_joinArray, $value);

        return $this;
    }
    
    public function data(array $value) {
        $this->_data = array_merge($this->_data, $value);
        
        return $this;
    }

    public function getList() {
        $stmt = Application::$db->prepare($this->stringify());

        if ($this->_limit) $stmt->bindValue(':limit', (int)$this->_limit, PDO::PARAM_INT);
        if ($this->_skip) $stmt->bindValue(':skip', (int)$this->_skip, PDO::PARAM_INT);

        foreach ($this->_data as $key => $value) {
            $stmt->bindValue(':' . $key, $value);
        }
        
		if ($stmt->execute()) {
			$this->_class ? $stmt->setFetchMode(PDO::FETCH_CLASS, $this->_class) : $stmt->setFetchMode(PDO::FETCH_OBJ);
            return $stmt->fetchAll();
		}
		else {
            $error = [
                "message" => $stmt->errorInfo()[2],
                "query" => $this->stringify(),
                "number" => $stmt->errorInfo()[1],
                "code" => $stmt->errorInfo()[0]
            ];
			Application::$answer->Error($error, 500);
		}
    }

    public function getListCount() {
        $stmt = Application::$db->prepare($this->stringify(true));

        foreach ($this->_data as $key => $value) {
            $stmt->bindValue(':' . $key, $value);
        }
		
		if ($stmt->execute()) {
            return (int) $stmt->fetchColumn();
		}
		else {
            $error = [
                "message" => $stmt->errorInfo()[2],
                "query" => $this->stringify(),
                "number" => $stmt->errorInfo()[1],
                "code" => $stmt->errorInfo()[0]
            ];
			Application::$answer->Error($error, 500);
		}
    }

    public function getOne() {
        $this->_limit = 1;

        $stmt = Application::$db->prepare($this->stringify());

        if ($this->_limit) $stmt->bindValue(':limit', (int)$this->_limit, PDO::PARAM_INT);
        if ($this->_skip) $stmt->bindValue(':skip', (int)$this->_skip, PDO::PARAM_INT);

        foreach ($this->_data as $key => $value) {
            $stmt->bindValue(':' . $key, $value);
        }

		if ($stmt->execute()) {
			$this->_class ? $stmt->setFetchMode(PDO::FETCH_CLASS, $this->_class) : $stmt->setFetchMode(PDO::FETCH_OBJ);
            return $stmt->fetch();
		}
		else {
            $error = [
                "message" => $stmt->errorInfo()[2],
                "query" => $this->stringify(),
                "number" => $stmt->errorInfo()[1],
                "code" => $stmt->errorInfo()[0]
            ];
			Application::$answer->Error($error, 500);
		}
    }

    private function stringify($counting = false): string {
        if (!$this->_table) {
            Application::$answer->Error("Table name isn't set", 500);
        }

        $result = "";

        if (!$counting) {
            $select = implode(',', $this->_selectArray);
            
            $result = "SELECT {$select} FROM {$this->_table}";
        }
        else {
            $result = "SELECT COUNT(*) FROM {$this->_table}";
        }

        if (count($this->_joinArray)) {
            $join = implode(' ', $this->_joinArray);
            $result = $result . ' ' . $join;
        }

        if (count($this->_whereArray)) {
            $where = implode(' AND ', $this->_whereArray);
            $result = $result . ' WHERE ' . $where;
        }

        if (!$counting) {
            if (count($this->_orderByArray)) {
                $orderArray = [];
                foreach ($this->_orderByArray as $str) {

                    $desc = $this->startsWith($str, '-');
                    $val = ltrim($str, '-');
                    if (isset($this->_orderByMap[$val])) {
                        if ($desc) {
                            array_push($orderArray, $this->_orderByMap[$val] . ' DESC');
                        }
                        else {
                            array_push($orderArray, $this->_orderByMap[$val] . ' ASC');
                        }
                    }
                }

                if (count($orderArray)) {
                    $order = implode(',', $orderArray);
                    $result = $result . ' ORDER BY ' . $order;
                }
            }

            if ($this->_limit) {
                $result = $result . " LIMIT :limit";
            }

            if ($this->_skip) {
                $result = $result . " OFFSET :skip";
            }
        }

        return $result;
    }

	private function endsWith($str, $needle)
	{
		$length = strlen($needle);
		if ($length == 0) {
			return true;
		}

		return (substr($str, -$length) === $needle);
	}

	function startsWith($str, $needle)
	{
		$length = strlen($needle);
		return (substr($str, 0, $length) === $needle);
	}
}
?>