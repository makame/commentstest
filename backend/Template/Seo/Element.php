<!DOCTYPE html>
<html>
    <head>
        <meta charset="iso-8859-1"/>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
        <meta property="og:title" content="<?php echo $this->data->title; ?>"/>
        <meta property="og:description" content="<?php echo strip_tags($this->data->desc); ?>"/>
        <meta property="og:image" content="<?php echo $this->data->image; ?>"/>
        <meta property="og:site_name" content="<?php echo $this->data->siteName; ?>"/>
        <meta property="og:url" content="<?php echo $this->data->url; ?>"/>
        <meta property="og:type" content="article"/>
    </head>
    <body>
        <h1><?php echo $this->data->title; ?></h1>
        <p><?php echo $this->data->content; ?></p>
    </body>
</html>