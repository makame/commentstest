<?php namespace Core;

// A commonly answer is { result: object?, error: object?, count: int? } - In this way we use following class
class Answer
{
	// Headers for answer
	private $status;
	private $headers;
	private $allow;
	private $host;

	// Public serialized values
	public $result = null;
	public $count = null;
	public $error = null;

	// Default values
	function __construct() {
		// Default responce code
		$this->status = 200;

		// Default headers
		$this->headers = [
			"Content-Type: application/json; charset=iso-8859-1"
		];

		// Default CORS
		$this->allow = ["*"];
		$this->host = "localhost";
	}

	// Set headers answer of the program
	private function finalizeHeader()
	{
		// Write CORS
		$origins = implode(",", $this->allow);
		
		header("Access-Control-Allow-Headers: {$origins}");
		header("Access-Control-Allow-Origin: {$this->host}");

		// Write headers
		foreach ($this->headers as $header) {
			header($header);
		}
		
		// Write responce code
		http_response_code($this->status);
	}

	// Finalaze answer of the program
	private function finalize()
	{
		$this->finalizeHeader();

		// Write answer body
		$result = (string)$this;
		echo $result;
		
		die();
	}

	// Throw Error to client
	public function Error($value = true, $status = 400)
	{
		$this->error = $value;
		$this->status = $status;
		$this->finalize();
	}

	// Throw Result to client
	public function Result($value = true, $count = null, $status = 200)
	{
		$this->result = $value;
		$this->count = $count;
		$this->status = $status;
		$this->finalize();
	}

	public function View($class, $data) {
		$className = 'View\\' . $class;
		if (class_exists($className)) {
			$view = new $className($data);
			$view->Render();
		}
		else {
			$view = new View($data);
			header('Content-Type: text/html;charset=iso-8859-1');
			$view->RenderTemplate($this->Template($class));
		}
	}

	public function Template($class) {
		$class = 'Template\\' . $class;
		return Application::$config->src . '/' . str_replace('\\', '/', $class) . '.php';
	}

	// Check method in array of args
	public function Allow()
	{
		$args = func_get_args();

		$this->allow = $args;

		// Commonly the first browser question is wich OPTIONS is awalable
		if (Application::$question->_method === 'OPTIONS') {
			$this->status = 200;
			$this->finalizeHeader();
			die();
		}

		// If method isn't allowed -> reject
		if (!in_array(Application::$question->_method, $args)) {
			$this->Error('Method Not Allowed', 405);
		}
	}

	// Overwrite toString method
	public function __toString(): string
	{
		return json_encode($this);
	}
}

?>