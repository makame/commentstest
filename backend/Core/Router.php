<?php namespace Core;

// The router implements MVC structure
Class Router {
	// Get a file name, controller name, action name, args
	private function getController(array $parts, &$controller, &$action) {
		if (count($parts) > 1) {
			$controllers = [];
			while (count($parts) > 0) {
				if (count($parts) > 1) {
					array_push($controllers, ucfirst(strtolower(trim($parts[0]))));
				}
				else {
					$action = ucfirst(strtolower(trim($parts[0])));
				}
				array_shift($parts);
			}
			$controller = implode('\\', $controllers);
		}
		else if (count($parts) > 1) {
			$controller = ucfirst(strtolower(trim($parts[0])));
			$action = 'Index';
		}
		else {
			$controller = 'Index';
			$action = 'Index';
		}
	}

	// Main()
	function run() {
		// Catch errors
		set_error_handler(function($errno, $errstr, $errfile, $errline) {
			$error = [
				"message" => $errstr,
				"number" => $errno,
				"file" => $errfile,
				"line" => $errline
			];
			Application::$answer->Error($error, 500);
		});

		// Get the url
		$url = $_SERVER["REQUEST_URI"];

		// Check if thr url starts with a base url
		if (substr($url, 0, strlen(Application::$config->base)) !== Application::$config->base) {
			Application::$answer->Error("404 Base isn't Found", 404);
		}

		// Clear the base url from a url start
		$url = substr($url, strlen(Application::$config->base));
		if ($q = strpos($url, '?')) {
			$url = substr($url, 0, $q);
		}

		// Split the url to the array by '/'
		$parts = array_filter(explode('/', $url));

		// Get a file name, controller name, action name, args
		$this->getController($parts, $controllerName, $actionName);

		// Set a controller class name
		$class = 'Controller\\' . $controllerName;

		// Get the class by a controller class name
		$controller = new $class();
		
		// Check an action exists
		if (is_callable(array($controller, $actionName)) == false) {
			Application::$answer->Error("404 Action isn't Found", 404);
		}
		
		// Run the action or thow an error
		try {
			$controller->$actionName();
		} catch (Exception $e) {
			Application::$answer->Error($e, 500);
		}
	}
}
?>