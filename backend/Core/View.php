<?php namespace Core;

// Parent class (interface) for all View classes
class View {
	protected $data;

	function __construct($data) {
		$this->data = $data;
	}

	function RenderTemplate($name) {
		include $name;
	}
}
?>