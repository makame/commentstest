<?php namespace Core;

// use Core\RegistryData;
// use Core\Answer;
// use Core\Question;
// use Core\Router;

// Application class allows to operate MVC structure and declares global varialables
class Application {
	// Contains config varialables
	public static $config;
	// Contains pdo object
	public static $db;

	// Answer is used to thow results & exceptions to client (Response)
	public static $answer;
	// Question is used to get client data (Request)
	public static $question;

	// Anti Flood protector
	public static $flood;

	// MVC router
	public static $router;

	// Init static varialables
	public static function init()
	{
		Application::$config = new RegistryData();
		Application::$answer = new Answer();
		Application::$question = new Question();
		Application::$router = new Router();
		Application::$flood = new AntiFlood();
	}
}
?>