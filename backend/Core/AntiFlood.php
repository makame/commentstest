<?php namespace Core;

// A commonly answer is { result: object?, error: object?, count: int? } - In this way we use following class
class AntiFlood
{
	private function antiflood_countaccess($ident, $ctrlFile, $timeout, $maxreq, $lockFile) {
		// counting requests and last access time
		$control = Array();

		if (file_exists($ctrlFile)) {
			$fh = fopen($ctrlFile, "r");
			$control = array_merge($control, unserialize(fread($fh, filesize($ctrlFile))));
			fclose($fh);
		}

		if (isset($control[$ident])) {
			if (time() - $control[$ident]["t"] < $timeout) {
				$control[$ident]["c"]++;
			} else {
				$control[$ident]["c"] = 1;
			}
		} else {
			$control[$ident]["c"] = 1;
		}
		
		$control[$ident]["t"] = time();

		if ($control[$ident]["c"] >= $maxreq) {
			// this user did too many requests within a very short period of time
			$fh = fopen($lockFile, "w");
			fwrite($fh, $ident);
			fclose($fh);
		}

		// writing updated control table
		$fh = fopen($ctrlFile, "w");
		fwrite($fh, serialize($control));
		fclose($fh);
	}

	private function ban($lockFile, $bantime) {
		if (file_exists($lockFile)) {
			if (time() - filemtime($lockFile) > $bantime) {
				// this user has complete his punishment
				unlink($lockFile);
			} else {
				// too many requests
				touch($lockFile);
				Application::$answer->Error('Many request, try later', 405);
			}
		}
	}

	public function Check($group = 'global', $bantime = 5, $maxreq = 5, $timeout = 1) {
		$ident = $group . $_SERVER["REMOTE_ADDR"];
		$ctrlFile = Application::$config->temp . '/flood/ctrl.db';
		$lockDir = Application::$config->temp . '/flood/lock';
		$lockFile = $lockDir . '/' . md5($ident);

		$this->ban($lockFile, $bantime);
		$this->antiflood_countaccess($ident, $ctrlFile, $timeout, $maxreq, $lockFile);
	}
}

?>