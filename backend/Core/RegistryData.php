<?php namespace Core;

// A Setter & Getter class which can't allow to change own Variables 
class RegistryData {
	// Variables' keeper
	private $vars = [];

	// Getter
	public function __get(string $attr)
	{
		if (isset($this->vars[$attr]))
			return $this->vars[$attr];
		else 
			Application::$answer->Error("Registry attibute {$attr} isn't found", 500);
	}

	// Setter
	public function __set(string $attr, $value)
	{
		if (isset($this->vars[$attr])) {
			Application::$answer->Error("Unable to set var {$attr}. Already set.", 500);
		}
		else {
			$this->vars[$attr] = $value;
		}
	}
}
?>