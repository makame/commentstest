<?php namespace Core;

// The class can get client data if it's a POST or a GET... 
class Question
{
	private $data;
	private $method;

	// Default init motod & data
	function __construct()
	{
		$this->method = $this->getMethod();
		$this->data = $this->getData();
	}

	// Getter
	public function __get(string $attr)
	{
		if ($attr === '_method') {
			return $this->method;
		}
		else if (isset($this->data[$attr])) {
			return $this->data[$attr];
		}
		else {
			Application::$answer->Error("The request don't contain '{$attr}' required field");
		}
	}

	// Getter
	public function __call(string $attr, $args)
	{
		if (isset($this->data[$attr])) {
			return $this->data[$attr];
		}
		else {
			$this->data[$attr] = $args[0];
			return $args[0];
		}
	}

	// Setter
	public function __set(string $attr, $value)
	{
		Application::$answer->Error('Setting attibutes isn\'t allowed', 500);
	}

	// Read POST from a body or GET data from a url
	private function getData()
	{
		if ($this->method === 'POST') {
			$result = json_decode(file_get_contents('php://input'), true);
			return is_string($result) ? [] : $result;
		}
		else {
			return $_GET;
		}
	}

	// Get method from REQUEST_METHOD
	private function getMethod(): string
	{
		return $_SERVER['REQUEST_METHOD'];
	}
}
?>