<?php namespace Core;

// Parent class (interface) for all Controller classes
abstract class Controller {
	function __construct() {
	}

	// Index function for a root page
	abstract function Index();
}
?>