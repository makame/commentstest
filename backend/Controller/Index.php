<?php namespace Controller;

use Core\Application;
use Core\Controller;

// Default route "/{base}/{api}"
class Index extends Controller
{
	// The default route /{base}/{api}
	public function Index() {
		$data = new \stdClass();
		$data->title = 'Home';
		$data->desc = 'Home';
		$data->content = 'Home';
		$data->image = null;
		$data->siteName = 'Comments';
		$data->url = '/hello';

		Application::$answer->View('Seo\\Element', $data);
	}
}
?>