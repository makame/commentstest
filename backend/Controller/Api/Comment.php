<?php namespace Controller\Api;

// IncludeManager::includeProject('Modules/Security');
// IncludeManager::includeProject('Modules/Comment');
// IncludeManager::includeProject('Modules/CommentSecure'); // Add info about spam --- IF U COMMENT IT then module will be switched
// IncludeManager::includeProject('Modules/CommentAuthor'); // Add info about user

use Core\Application;
use Core\Controller;
use Module\Security\Session;
use Module\Comment\Model;

// Comment "/{base}/{api}/Comment"
class Comment extends Controller
{
	public function Index() {
		Application::$answer->Allow();
	}

	// Get a list of comments "/{base}/{api}/Comment/GetList"
	public function GetList() {
		// Setting allow methods
		Application::$answer->Allow('GET', 'POST');
		
		// Checking flood
		Application::$flood->Check('comment');
		
		// Getting the session
		$session = Session::check();

		if (strlen(Application::$question->limit(10)) < 0) {
			Application::$answer->Error('Limit should be more than 0');
		}

		if (strlen(Application::$question->skip(0)) < 0) {
			Application::$answer->Error('Skip should be more than 0');
		}

		// Getting the user
		$comments = Model\Comment::getList(
			Application::$question->join([]),
			Application::$question->order(['-date']),
			Application::$question->limit(10),
			Application::$question->skip(0)
		);

		// Getting the user
		$commentsCount = Model\Comment::getListCount();
		
		// Returning the user
		Application::$answer->Result($comments, $commentsCount);
	}

	// Get a count of new comments "/{base}/{api}/Comment/CountNew"
	public function CountNew() {
		// Setting allow methods
		Application::$answer->Allow('GET', 'POST');
		
		// Checking flood
		Application::$flood->Check('comment', 5, 60);
		
		// Getting the session
		$session = Session::check();

		$count = Model\Comment::getNewCount(Application::$question->date);

		// Getting the user
		Application::$answer->Result($count > 0, $count);
	}

	// Create a new comment "/{base}/{api}/Comment/CreateMessage"
	public function CreateMessage() {
		// Setting allow methods
		Application::$answer->Allow('GET', 'POST');
		
		// Checking flood
		Application::$flood->Check('comment', 10, 1);
		
		// Getting the session
		$session = Session::check();

		if (strlen(Application::$question->message) < 6 || strlen(Application::$question->message) > 1000) {
			Application::$answer->Error('Message should be more than 5 and less than 1000');
		}

		// Getting the user
		$comment = Model\Comment::create(
			$session->userId,
			Application::$question->message,
			Application::$question->join([])
		);
		
		// Returning the user
		Application::$answer->Result($comment);
	}

	// Ceck Comment "/{base}/{api}/Comment/CheckMessage"
	public function CheckMessage() {
		// Setting allow methods
		Application::$answer->Allow('GET', 'POST');
		
		// Checking flood
		Application::$flood->Check('comment');
		
		// Getting the session
		$session = Session::check();
		// Getting the user
		$comment = Model\Comment::get(
			[],
			Application::$question->id
		);

		Model\Comment::check($comment);
		
		$comment = Model\Comment::get(
			Application::$question->id,
			Application::$question->join([])
		);

		// Returning the result
		Application::$answer->Result($comment);
	}
}
?>