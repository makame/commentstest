<?php namespace Controller\Api;

use Core\Application;
use Core\Controller;
use Module\Security\Session;
use Module\Security\Model;

// Auth "/{base}/{api}/auth"
class Auth extends Controller
{
	// Checking the user session & return the user from DB "/{base}/{api}/auth"
	public function Index() {
		// Setting allow methods
		Application::$answer->Allow('GET', 'POST');

		// Checking flood
		Application::$flood->Check();
		
		// Getting the session
		$session = Session::check();

		// Getting the user
		$user = Model\User::get(
			$session->userId,
			Application::$question->join([])
		);
		
		// Returning the user
		Application::$answer->Result($user);
	}

	// Login "/{base}/{api}/auth/login"
	public function Login()
	{
		// Setting allow methods
		Application::$answer->Allow('POST');

		// Checking flood
		Application::$flood->Check('auth', 60, 3);

		// Checking a username field
		if (strlen(Application::$question->username) < 5)
		{
			Application::$answer->Error("Username is less than 5 chars");
		}
		
		// Checking a password field
		if (strlen(Application::$question->password) < 5)
		{
			Application::$answer->Error("Password is less than 5 chars");
		}
		
		// Trying to get a new user
		$user = Model\User::getWithUsernamePassword(
			Application::$question->username,
			md5(Application::$question->password . Application::$config->secretKey),
			Application::$question->join([])
		);
		
		// Checking the user
		if (!$user)
		{
			Application::$answer->Error("The user doesn't exist");
		}

		// Creating a session
		Session::create($user->id);
		
		// Return the user
		Application::$answer->Result($user);
	}

	// User registration "/{base}/{api}/auth/registration"
	public function Registration()
	{
		// Set allow methods
		Application::$answer->Allow('POST');

		// Checking flood
		Application::$flood->Check('auth', 60, 3);

		// Checking an username field
		if (strlen(Application::$question->username) < 5 || strlen(Application::$question->username) > 10) {
			Application::$answer->Error('Username should be more than 4 and less than 10');
		}
		
		// Checking a password field
		if (strlen(Application::$question->password) < 5 || strlen(Application::$question->password) > 10) {
			Application::$answer->Error('Password should be more than 4 and less than 10');
		}

		// Creating an user
		$user = Model\User::create(
			Application::$question->username,
			md5(Application::$question->password . Application::$config->secretKey),
			Application::$question->join([])
		);
		
		// Return the user
		Application::$answer->Result($user);
	}
	
	// Logout "/{base}/{api}/auth/logout"
	public function Logout()
	{
		// Set allow methods
		Application::$answer->Allow('GET', 'POST');

		// Deleting the session
		Session::delete();

		// Return true
		Application::$answer->Result();
	}
}
?>