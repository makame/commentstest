<?php namespace Controller\Api;

use Core\Application;
use Core\Controller;

// Default route "/{base}/{api}"
class Index extends Controller
{
	// The default route /{base}/{api}
	public function Index() {
		// Application::$answer->Result("API v1.0");
		$swagger = \Swagger\scan([Application::$config->dir]);
		header('Content-Type: application/json');
		echo $swagger;
		die;
	}
}
?>