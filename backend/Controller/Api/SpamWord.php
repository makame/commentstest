<?php namespace Controller\Api;

use Core\Application;
use Core\Controller;
use Module\Security\Session;
use Module\CommentSecure\Model;

// Auth "/{base}/{api}/SpamWord"
class SpamWord extends Controller
{
	public function Index() {
		Application::$answer->Allow();
	}

	// Get a list of words "/{base}/{api}/SpamWord/GetList"
	public function GetList() {
		// Setting allow methods
		Application::$answer->Allow('GET', 'POST');
		
		// Checking flood
		Application::$flood->Check('spamword');
		
		// Getting the session
		$session = Session::check();

		if (strlen(Application::$question->limit) < 0) {
			Application::$answer->Error('Limit should be more than 0');
		}

		if (strlen(Application::$question->skip) < 0) {
			Application::$answer->Error('Skip should be more than 0');
		}

		// Getting the list
		$words = Model\SpamWord::getList(
			Application::$question->join([]),
			Application::$question->order(['-date']),
			Application::$question->limit,
			Application::$question->skip
		);

		// Getting the count
		$wordsCount = Model\SpamWord::getListCount();
		
		// Returning
		Application::$answer->Result($words, $wordsCount);
	}

	// Create a new word "/{base}/{api}/SpamWord/CreateWord"
	public function CreateWord() {
		// Setting allow methods
		Application::$answer->Allow('GET', 'POST');
		
		// Checking flood
		Application::$flood->Check('spamword', 10, 1);
		
		// Getting the session
		$session = Session::check();

		if (strlen(Application::$question->word) < 2 || strlen(Application::$question->word) > 100) {
			Application::$answer->Error('Word should be more than 1 and less than 100');
		}

		// Create the word
		$word = Model\SpamWord::create(Application::$question->word, Application::$question->join([]));
		
		// Returning result
		Application::$answer->Result($word);
	}

	// Change a word "/{base}/{api}/SpamWord/ChangeWord"
	public function ChangeWord() {
		// Setting allow methods
		Application::$answer->Allow('GET', 'POST');
		
		// Checking flood
		Application::$flood->Check('spamword', 10, 1);
		
		// Getting the session
		$session = Session::check();

		// Getting
		$word = Model\SpamWord::get(
			Application::$question->id,
			Application::$question->join([])
		);
		$word->word = Application::$question->word;
		$word->update();
		
		// Returning resulr
		Application::$answer->Result($word);
	}

	// Delete a word "/{base}/{api}/SpamWord/DeleteWord"
	public function DeleteWord() {
		// Setting allow methods
		Application::$answer->Allow('GET', 'POST');
		
		// Checking flood
		Application::$flood->Check('spamword');
		
		// Getting the session
		$session = Session::check();

		// Getting the word
		$word = Model\SpamWord::get(
			Application::$question->id,
			Application::$question->join([])
		);
		
		// Returning result of deleting
		Application::$answer->Result($word->delete());
	}
}
?>