<?php
// Only for development
ini_set('display_errors', 1);
error_reporting(E_ALL);

//Check version of PHP
if (version_compare(phpversion(), '7.0.0', '<') == true) { die ('PHP > 7.0 Only'); }

include "backend/vendor/autoload.php";

use Core\Application;

// Declare global register
Application::init();

// Declare global varialables
Application::$config->dir = __DIR__;
Application::$config->src = __DIR__ . '/backend';
Application::$config->base = '/';
Application::$config->cookie = '/';
Application::$config->api = 'api';
Application::$config->secretKey = 'prhome';
Application::$config->temp = __DIR__ . '/' . 'temp';

// Initialize PDO
Application::$db = new PDO('mysql:host=127.0.0.1;dbname=phpdb', 'root', '');

// If we use cyrilic for the most SQLs
// Application::$db->exec('SET NAMES utf8');

// registry comments delegates
ORM\Event::registry(Module\Comment\Model\Comment::$event, new Module\CommentAuthor\CommentAuthorDelegate());
ORM\Event::registry(Module\Comment\Model\Comment::$event, new Module\CommentSecure\CommentSecureDelegate());

/**
 * @SWG\Swagger(
 *     @SWG\Info(
 *         version="1.0.0",
 *         title="Swagger Petstore",
 *         @SWG\License(name="MIT")
 *     ),
 *     host="localhost",
 *     basePath="/api",
 *     schemes={"http"},
 *     consumes={"application/json"},
 *     produces={"application/json"},
 *     @SWG\Definition(
 *         definition="Error",
 *         required={"code", "message"},
 *         @SWG\Property(
 *             property="code",
 *             type="integer",
 *             format="int32"
 *         ),
 *         @SWG\Property(
 *             property="message",
 *             type="string"
 *         )
 *     ),
 *     @SWG\Definition(definition="Pets",
 *         type="array",
 *         @SWG\Items(ref="#/definitions/Pet")
 *     )
 * )
 */
 
// Get controller and run its action from router
Application::$router->run();
?>