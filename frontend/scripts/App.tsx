import * as React from 'react';
import { render } from 'react-dom';
// var eqcss = require('eqcss');

const Velocity = require('velocity-animate');
$.fn.velocity = Velocity;
// $.fn.reverse = [].reverse;

MutationObserver = (window as any).MutationObserver || (window as any).WebKitMutationObserver;

var observer = new MutationObserver(function(mutations, observer) {
    // $('.popup').parent().bind('hover', (e) => {
    //   $(e.target).css('z-index', 10);
    //   $(e.target).parents().css('z-index', 10);
    // })
    $('.popup').each((i, e)=>{
      var popup = $(e);
      var parent = $(e).parent();
      var input = popup.prev('input');
      var operator = input.length > 0 ? input : parent;

      var show = () => {
          popup.parents().css('z-index', 10);
          popup.addClass('show');
          operator.addClass('hover');
      }

      var hide = () => {
          if (!popup.hasClass('-focused') && !popup.hasClass('-hover') && !operator.is(":focus") && !operator.is(":hover")) {
            popup.parents().css('z-index', 0);
            popup.removeClass('show');
            operator.removeClass('hover');
          }
      }

      if (!operator.hasClass('-ed')) {
        operator.bind('mouseover focus', (e) => {
          show();
        });
        
        operator.bind('mouseleave blur', (e) => {
          hide();
        });

        if (input.length == 0) {
          operator.bind('click', (e) => {
            var inside = popup.find('input').first();
            if (inside.length > 0) {
              inside.focus();
            }
          });
        }

        operator.addClass('-ed');
      }

      if (!popup.hasClass('-ed')) {
        popup.bind('mouseover', (e) => {
          popup.addClass('-hover');
          show();
        });
        
        popup.bind('mouseleave', (e) => {
          popup.removeClass('-hover');
          hide();
        });

        popup.addClass('-ed');
      }

      popup.children('*').each((i, e) => {
        if (!$(e).hasClass('-ed')) {
          $(e).focusin(() => {
            popup.addClass('-focused');
            show();
          });
          
          $(e).focusout(() => {
            popup.removeClass('-focused');
            hide();
          });

          $(e).addClass('-ed');
        }
      });
    });
});

// define what element should be observed by the observer
// and what types of mutations trigger the callback
observer.observe(document, {
  subtree: true,
  attributes: true,
  childList: true,
  characterData: true
});

(function() {
  var ev = new $.Event('display');
  var orig = $.fn.css;

  $.fn.css = function() {
      orig.apply(this, arguments);
      $(this).trigger(ev);
  }
})();


export class ApplicationProps {
}

export class ApplicationState {
}

//hookup the event

//show div and trigger custom event in callback when div is visible
// $('.popup').show('slow', function(){
//     $(this).trigger('isVisible');
// });

export class Application extends React.Component<ApplicationProps, ApplicationState> {
  didRender() {
  }

  componentDidMount(prevProps, prevState) {
    this.didRender();
  }

  componentDidUpdate(prevProps, prevState) {
    this.didRender();
  }

  render() {
    return <div>
      <div className="sp-menu-container">
        <div className="sp-menu">
          <div>
            {/*<div className="sp-header">
              <span className="fa fa-asterisk"/>
            </div>*/}
            <div className="sp-item">
              {/*<span className="fa fa-circle-o"/>*/}
              Блог
              <span className="popup bottom">Интересные посты и увлекательные комментарии ждут вас</span>
            </div>
            <div className="sp-item">
              {/*<span className="fa fa-circle-o"/>*/}
              Рынок
              <span className="popup bottom">Чтобы продать или купить какой либо товар или услугу, не проходите мимо</span>
            </div>
            <div className="sp-item active">
              {/*<span className="fa fa-dot-circle-o"/>*/}
              Недвижимость
              <span className="popup bottom">Если вам нужно сдать или снять жилье, то вам сюда</span>
            </div>
            <div className="sp-item loading">
              {/*<span className="fa fa-circle-o"/>*/}
              Работа
              <span className="popup bottom">Обьявления по работе</span>
            </div>
            <div className="sp-item">
              {/*<span className="fa fa-circle-o"/>*/}
              Форум
              <span className="popup bottom">Вы сможете обсудить что-то именно тут</span>
            </div>
            <div className="sp-item">
              {/*<span className="fa fa-circle-o"/>*/}
              Модерация
              <span className="popup bottom">Модерация всей вашей учетной записи и ваших действий</span>
            </div>
            <div className="sp-input sp-space loading">
              <input type="text" placeholder="Search"/>
              <span className="fa fa-search"/>
              <span className="right">Enter to send</span>
              <span className="popup error bottom">Search string should be more than 5 chars
                Search string should be more than 5 chars
                Search string should be more than 5 chars<br/>
                Search string should be more than 5 chars
              </span>
            </div>
            <div className="sp-group">
              <div className="sp-item image active">
                <span className="popup bottom">Тур по странице</span>
                <span className="fa fa-question"/>
              </div>
              <div className="sp-divider"/>
              <div className="sp-item image">
                <span className="popup bottom">Чат и конференции</span>
                <span className="bud">12</span>
                <span className="fa fa-comment"/>
              </div>
              <div className="sp-divider"/>
              <div className="sp-item image">
                <span className="popup bottom">Ваш профиль</span>
                <span className="fa fa-user-md"/>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div className="container padding-bottom-3">

        <div className="">

          <h3 className="margin-top-2">Reports</h3>
          <div className="sp-tab margin-top-d1">
            <div>
              <div className="sp-item">
                Блог
                <span className="popup bottom">Интересные посты и увлекательные комментарии ждут вас</span>
              </div>
              <div className="sp-item">
                Рынок
                <span className="popup bottom">Чтобы прожать или купить какой либо товар напрямую, не проходите мимо</span>
              </div>
              <div className="sp-item active">
                Недвижимость
                <span className="popup bottom">Если вам нужно сдать или снять жилье, то вам сюда</span>
              </div>
              <div className="sp-item loading">
                Работа
                <span className="popup bottom">Обьявления по работе</span>
              </div>
              <div className="sp-item">
                Форум
                <span className="popup bottom">Вы сможете обсудить что-то именно тут</span>
              </div>
              <div className="sp-item disabled">
                Модерация
                <span className="popup bottom">Модерация всей вашей учетной записи и ваших действий</span>
              </div>
              <div className="sp-space"/>
              {/*<div className="sp-input sp-space loading">
                <input type="text" placeholder="Search"/>
                <span className="fa fa-search"/>
                <span className="right">Enter to send</span>
                <span className="popup error bottom">Search string should be more than 5 chars
                  Search string should be more than 5 chars
                  Search string should be more than 5 chars<br/>
                  Search string should be more than 5 chars
                </span>
              </div>
              <div className="sp-group">
                <div className="sp-item image active">
                  <span className="popup bottom">Связь с нами</span>
                  <span className="fa fa-info"/>
                </div>
                <div className="sp-divider"/>
                <div className="sp-item image">
                  <span className="popup bottom">Чат и конференции</span>
                  <span className="bud">12</span>
                  <span className="fa fa-comment"/>
                </div>
                <div className="sp-divider"/>
                <div className="sp-item image">
                  <span className="popup bottom">Ваш профиль</span>
                  <span className="fa fa-user-md"/>
                </div>
              </div>*/}
              <div className="padding-right-1">
                <button className="btn btn-primary btn-sm" tabIndex={0}><span className="popup left">Hello!</span>New Order</button>
              </div>
            </div>
          </div>

          <span className="sp-line margin-top-d2 margin-bottom-3"></span>

        </div>

        <div className="container-content">

          <div className="sp-progress margin-bottom-2">
            <div className="meter">
              First Step
            </div>
            <div className="meter">
              Second
            </div>
            <div className="meter active" data-progress="75%">
              Pre
            </div>
            <div className="meter">
              Final
            </div>
          </div>

          <div className="sp-pane padding-2">
            <div className="sp-form-row">
              <span className="item fa fa-at text subsub padding-left-d2 padding-right-1"/>
              <input type="text" placeholder="Email"/>
              <span className="group">
                <span className="item fa fa-globe text subsub padding-left-1 padding-right-1"/>
                <input type="text" placeholder="Region"/>
                <span className="popup select bottom">
                  <div className="select-container">
                    <button className="sp-item" tabIndex={0}>Открыть профиль</button>
                    <button className="sp-item" tabIndex={0}>Пожаловаться</button>
                  </div>
                </span>
                <span className="next"/>
              </span>
              <span className="next"/>
            </div>

            <div className="sp-form-row margin-top-1">
              <span className="item fa fa-at text subsub padding-left-d2 padding-right-1"/>
              <input type="text" placeholder="Email"/>
              <span className="popup select bottom">
                <div className="select-container">
                  <button className="sp-item" tabIndex={0}>Открыть профиль</button>
                  <button className="sp-item" tabIndex={0}>Пожаловаться</button>
                </div>
              </span>
              <span className="next"/>
            </div>

            <div className="sp-form-row margin-top-1">
              <span className="item fa fa-at text subsub padding-left-d2 padding-right-1"/>
              <span className="selecting">
                {/*<span className="result">Searched Input</span>*/}
                <span className="placeholder">Placeholder</span>
                <span className="popup select bottom">
                  <div className="sp-input">
                    <input type="text" placeholder="Search"/>
                    <span className="fa fa-search"/>
                    <span className="right">Enter to send</span>
                  </div>
                  <div className="select-container">
                    <button className="sp-item" tabIndex={0}>Открыть профиль</button>
                    <button className="sp-item" tabIndex={0}>Пожаловаться</button>
                  </div>
                </span>
              </span>
              <span className="next"/>
            </div>

            <div className="sp-form-row loading margin-top-1">
              <span className="item fa fa-at text subsub padding-left-d2 padding-right-1"/>
              <input type="text" placeholder="Email"/>
              <span className="next"/>
            </div>

            <div className="sp-form-row margin-top-1" disabled>
              <span className="item fa fa-at text subsub padding-left-d2 padding-right-1"/>
              <input type="text" placeholder="Email" disabled/>
              <span className="item fa fa-globe text subsub padding-left-1 padding-right-1"/>
              <input type="text" placeholder="Region" disabled/>
              <span className="popup select bottom">
                <div className="select-container">
                  <button className="sp-item" tabIndex={0}>Открыть профиль</button>
                  <button className="sp-item" tabIndex={0}>Пожаловаться</button>
                </div>
              </span>
              <span className="next"/>
            </div>
          </div>

          <div className="sp-pane">
            <div className="pane-table">
              <div className="col width-1 padding-2 padding-right-d2">
                <img className="sp-profile-img" src="/images/avatar-empty.png"/>
              </div>
              <div className="col width-1 middle padding-vertical-2">
                <div className="text link hover nowrap big thick">
                  makame
                  <span className="fa fa-bars text large subsub margin-left-d1"/>
                  <span className="popup select bottom">
                    <div className="select-container">
                      <button className="sp-item" tabIndex={0}>Открыть профиль</button>
                      <button className="sp-item" tabIndex={0}>Пожаловаться</button>
                    </div>
                  </span>
                </div>
                <div className="text subsub"><span className="fa fa-caret-up text green"/>168</div>
              </div>
              <div className="col middle padding-vertical-2 padding-horizontal-2">
                <div>
                  <div className="sp-tag item hover" tabIndex={0}>
                    <img className="sp-profile-img-tag" src="/images/avatar-empty.png"/>
                    маврикий
                    <span className="bars"/>
                    <span className="popup select bottom">
                      <div className="select-container">
                        <button className="sp-item" tabIndex={0}>Открыть профиль</button>
                        <button className="sp-item" tabIndex={0}>Пожаловаться</button>
                      </div>
                    </span>
                  </div>
                  <div className="sp-tag item">маврикий<span className="close"/></div>
                  <div className="sp-tag">красота</div>
                </div>
                <div className="text subsub padding-horizontal-2">Тэги</div>
              </div>
            </div>
            <div className="pane-table">
              <div className="row border-top">
                <div className="col center border-right padding-vertical-d2 padding-horizontal-4 width-1">
                  <div className="text big thick ellipsis"><span className="fa fa-caret-up text green"/>180</div>
                  <div className="text subsub">Рейтинг</div>
                </div>
                <div className="col item center middle border-right width-2 padding-vertical-1 padding-horizontal-2 active">
                  <span className="fa fa-caret-up text green big"/>
                  <div className="text subsub">За</div>
                </div>
                <div className="col item center middle border-right width-2 padding-vertical-1 padding-horizontal-2">
                  <span className="fa fa-caret-down text red big"/>
                  <div className="text subsub">Против</div>
                </div>
                <div className="col center border-right padding-vertical-d2 padding-horizontal-4 width-1">
                  <div className="text big thick">186</div>
                  <div className="text subsub">Просмотров</div>
                </div>
                <div className="col center padding-vertical-d2 padding-horizontal-4">
                  <div className="text large thick">17/02/2016</div>
                  <div className="text subsub">Дата</div>
                </div>
                <div className="col center border-left padding-vertical-d2 padding-horizontal-4">
                  <div className="text large thick green">Проверен</div>
                  <div className="text subsub">Статус</div>
                </div>
                <div className="col item hover center middle border-left width-1 padding-vertical-1 padding-horizontal-2">
                  <span className="fa fa-bars text big subsubsub"/>
                  <span className="popup select left">
                    <div className="select-container">
                      <button className="sp-item" tabIndex={0}>Подтвердить статус</button>
                      <button className="sp-item" tabIndex={0}>Отказать</button>
                      <button className="sp-item" tabIndex={0}>Проверить статус</button>
                      <button className="sp-item" tabIndex={0}>Изменить</button>
                      <button className="sp-item" tabIndex={0}>Удалить</button>
                      <button className="sp-item" tabIndex={0}>Пожаловаться</button>
                    </div>
                  </span>
                </div>
              </div>
            </div>
          </div>

          <div className="sp-pane padding-2 margin-top-d2">
            <div className="margin-bottom-1 text big thick">Hello WORLD! Server!</div>
            <div>Hello WORLD! Server!</div>
          </div>

          <div className="sp-pane padding-2 margin-top-d2">
            <div className="margin-bottom-1 text medium thick">Hello WORLD! Server!</div>
            <div>Hello WORLD! Server!</div>
            <div className="margin-bottom-1 margin-top-d2 text medium thick">Hello WORLD! Server!</div>
            <div>Hello WORLD! Server!</div>
          </div>
        
        </div>

        <div className="table container-fluid margin-top-d2">
          <div className="row header">
            <div className="col ellipsis center item" style={{'width': '2rem'}}>
              <span className="fa fa-square text subsubsub"/>
            </div>
            <div className="col item hover">
              Description
              <span className="fa fa-sort margin-left-d1"/>
              <span className="popup select top">
                <div className="sp-input">
                  <input type="text" placeholder="Search"/>
                  <span className="fa fa-search"/>
                  <span className="right">Enter to send</span>
                  <span className="popup error top">
                    Search string should be more than 5 chars
                    Search string should be more than 5 chars
                    Search string should be more than 5 chars<br/>
                    Search string should be more than 5 chars
                  </span>
                </div>
                <div className="select-container">
                  <div className="sp-item active">
                    <span className="fa fa-sort-amount-asc margin-right-d1"/>
                    По возрастанию
                  </div>
                  <div className="sp-item">
                    <span className="fa fa-sort-amount-desc margin-right-d1"/>
                    По убыванию
                  </div>
                </div>
              </span>
            </div>
            <div className="col ellipsis center" style={{'width': '5rem'}}>
              Actions
            </div>
          </div>
          <div className="row-table">
            <div className="row border">
              <div className="col padding-vertical-1 border-right item middle center" style={{'width': '2rem'}}>
                <span className="fa fa-square text medium subsubsub"/>
              </div>
              <div className="col ellipsis border-right">
                Hello 2
                Hello 1
                Hello 1
                Hello 1
                Hello 1
                Hello 1
              </div>
              <div className="col padding-vertical-1 item hover middle center" style={{'width': '5rem'}}>
                <span className="fa fa-bars text big subsubsub"/>
                <span className="popup select left">
                  <div className="select-container">
                    <button className="sp-item" tabIndex={0}>Проверить статус</button>
                    <button className="sp-item" tabIndex={0}>Изменить</button>
                    <button className="sp-item" tabIndex={0}>Удалить</button>
                  </div>
                </span>
              </div>
            </div>
            <div className="row border">
              <div className="col padding-vertical-1 border-right item middle center" style={{'width': '2rem'}}>
                <span className="fa fa-check-square text medium blue"/>
              </div>
              <div className="col ellipsis border-right">
                Hello 2
                Hello 1
                Hello 1
                Hello 1
                Hello 1
                Hello 1
              </div>
              <div className="col padding-vertical-1 item hover middle center" style={{'width': '5rem'}}>
                <span className="fa fa-bars text big subsubsub"/>
                <span className="popup select left">
                  <div className="sp-input">
                    <input type="text" placeholder="Search"/>
                    <span className="fa fa-search"/>
                    <span className="right">Enter to send</span>
                    <span className="popup error left">Search string should be more than 5 chars
                      Search string should be more than 5 chars
                      Search string should be more than 5 chars<br/>
                      Search string should be more than 5 chars
                    </span>
                  </div>
                  <div className="select-container">
                    <div className="sp-item">Проверить статус</div>
                    <div className="sp-item loading">Изменить</div>
                    <div className="sp-item">Удалить</div>
                    <div className="sp-item disabled">Проверить статус</div>
                    <div className="sp-item">Изменить</div>
                    <div className="sp-item">Удалить</div>
                    <div className="sp-item">Проверить статус</div>
                    <div className="sp-item">Изменить</div>
                    <div className="sp-item">Удалить</div>
                    <div className="sp-item">Проверить статус</div>
                    <div className="sp-item">Изменить</div>
                    <div className="sp-item">Удалить</div>
                    <div className="sp-item">Проверить статус</div>
                    <div className="sp-item">Изменить</div>
                    <div className="sp-item">Удалить</div>
                    <div className="sp-item">Проверить статус</div>
                    <div className="sp-item">Изменить</div>
                    <div className="sp-item">Удалить</div>
                    <div className="sp-item">Проверить статус</div>
                    <div className="sp-item">Изменить</div>
                    <div className="sp-item">Удалить</div>
                    <div className="sp-item">Проверить статус</div>
                    <div className="sp-item">Изменить</div>
                    <div className="sp-item">Удалить</div>
                    <div className="sp-item">Проверить статус</div>
                    <div className="sp-item">Изменить</div>
                    <div className="sp-item">Удалить</div>
                    <div className="sp-item">Проверить статус</div>
                    <div className="sp-item">Изменить</div>
                    <div className="sp-item">Удалить</div>
                    <div className="sp-item">Проверить статус</div>
                    <div className="sp-item">Изменить</div>
                    <div className="sp-item">Удалить</div>
                  </div>
                </span>
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col ellipsis item" style={{'width': '2rem'}}>
              Hello 1
            </div>
            <div className="col">
              Hello 2
            </div>
            <div className="col middle center border-left padding-vertical-1" style={{'width': '5rem'}}>
              <button className="btn xs" tabIndex={0}>Test Me!</button>
            </div>
          </div>
        </div>

        <button className="pane-btn margin-top-1">Show More</button>

        <div className="sp-pagination margin-top-1">
          <button className="sp-item">1</button>
          <button className="sp-item">2</button>
          <button className="sp-item disabled">...</button>
          <button className="sp-item">4</button>
          <button className="sp-item active">5</button>
          <button className="sp-item">6</button>
          <button className="sp-item disabled">...</button>
          <button className="sp-item">10</button>
          <button className="sp-item">11000</button>
        </div>

        <h3 className="margin-top-4">Hello WORLD! Server!</h3>
        <div>Hello WORLD! Server!</div>
        <h3>Hello WORLD! Server!</h3>
        <div>Hello WORLD! Server!</div>

        <button className="btn btn-default" tabIndex={0} onClick={()=>{}}><span className="popup top">Hello!</span>Test Me!</button>
        <button className="btn" tabIndex={0}>Test Me!</button>
        <button className="btn error" tabIndex={0}>Test Me!<span className="popup error top">Search string should be more than 5 chars</span></button>
        <button className="btn loading" tabIndex={0}>Test Me!</button>
        <button className="btn disabled" tabIndex={0}>Test Me!</button>
        <button className="btn btn-primary error" tabIndex={0}>Test Me!</button>
        <button className="btn btn-primary loading" tabIndex={0}>Test Me!</button>
        <button className="btn btn-primary disabled" tabIndex={0}>Test Me!</button>
        <button className="btn btn-danger" tabIndex={0}><span className="fa fa-home"/><span>Test Me!</span></button>

        <div className="sp-pane padding-2 margin-top-d2 text small">
          <div className="margin-bottom-d2">
            <span className="box-xs padding-horizontal-0 text lite sub">
              <span>325 комментариев, упорядочить по </span>
              <span className="text thin link hover">
                актуальности
                <span className="fa fa-bars margin-left-d1"/>
                <span className="popup select bottom">
                  <div className="select-container">
                    <div className="sp-item active">Актуальности</div>
                    <div className="sp-item">Рейтингу</div>
                    <div className="sp-item">Дате</div>
                  </div>
                </span>
              </span>
              <span> из них<span className="fa fa-caret-up text green margin-horizontal-d1"/>522 и<span className="fa fa-caret-down text red margin-horizontal-d1"/>43</span>
            </span>
            <button className="btn btn-xs btn-primary padding-horizontal-1 margin-left-d1" tabIndex={0}><span className="fa fa-reply text margin-right-d1"/>Ответить</button>
          </div>

          <div className="padding-bottom-d2-ntlast">
            <div className="margin-bottom-1">
              <span className="text lite sub margin-left-d1 margin-right-d1 box-xs padding-horizontal-0"><span className="fa fa-caret-up text green margin-right-d1"/>15</span>
              <button className="btn btn-xs padding-horizontal-1" tabIndex={0}><span className="fa fa-caret-up text green margin-right-d1"/>За</button>
              <button className="btn btn-xs padding-horizontal-1" tabIndex={0}><span className="fa fa-caret-down text red margin-right-d1"/>Против</button>
              <div className="sp-tag item hover margin-left-d1">
                <img className="sp-profile-img-tag" src="/images/avatar-empty.png"/>
                makame
                <div className="text subsub margin-left-d1"><span className="fa fa-caret-up text green margin-right-dd1"/>168</div>
                <span className="bars"/>
                <span className="popup select bottom">
                  <div className="select-container">
                    <div className="sp-item">Открыть профиль</div>
                    <div className="sp-item">Пожаловаться</div>
                  </div>
                </span>
              </div>
              <span className="text lite sub margin-left-d1 box-xs padding-horizontal-0">17/02/2016</span>
              <button className="btn btn-xs margin-left-d1 padding-horizontal-1" tabIndex={0}><span className="fa fa-chain text sub"/></button>
              <button className="btn btn-xs padding-horizontal-1" tabIndex={0}><span className="fa fa-save text sub"/></button>
              <button className="btn btn-xs hover padding-horizontal-1" tabIndex={0}>
                <span className="popup select bottom">
                  <div className="select-container">
                    <div className="sp-item">Удалить</div>
                    <div className="sp-item">Редактировать</div>
                    <div className="sp-item">Пожаловаться</div>
                  </div>
                </span>
                <span className="fa fa-bars text sub"/>
              </button>
            </div>
            <div className="margin-bottom-1">Hello WORLD! Server!</div>
            <div>
              <button className="btn btn-xs text sub padding-horizontal-1 margin-right-d1" tabIndex={0}><span className="fa fa-caret-down margin-right-d1"/>Развернуть (15)</button>
              <button className="btn btn-xs btn-primary padding-horizontal-1" tabIndex={0}><span className="fa fa-reply text margin-right-d1"/>Ответить</button>
            </div>
          </div>

          <div className="padding-bottom-d2-ntlast">
            <div className="margin-bottom-1">
              <span className="text lite sub margin-left-d1 margin-right-d1 box-xs padding-horizontal-0"><span className="fa fa-caret-up text green margin-right-d1"/>15</span>
              <button className="btn btn-xs padding-horizontal-1" tabIndex={0}><span className="fa fa-caret-up text green margin-right-d1"/>За</button>
              <button className="btn btn-xs padding-horizontal-1" tabIndex={0}><span className="fa fa-caret-down text red margin-right-d1"/>Против</button>
              <div className="sp-tag item hover margin-left-d1">
                <img className="sp-profile-img-tag" src="/images/avatar-empty.png"/>
                makame
                <div className="text subsub margin-left-d1"><span className="fa fa-caret-up text green margin-right-dd1"/>168</div>
                <span className="bars"/>
                <span className="popup select bottom">
                  <div className="select-container">
                    <div className="sp-item">Открыть профиль</div>
                    <div className="sp-item">Пожаловаться</div>
                  </div>
                </span>
              </div>
              <span className="text lite sub margin-left-d1 box-xs padding-horizontal-0">17/02/2016</span>
              <button className="btn btn-xs margin-left-d1 padding-horizontal-1" tabIndex={0}><span className="fa fa-chain text sub"/></button>
              <button className="btn btn-xs padding-horizontal-1" tabIndex={0}><span className="fa fa-save text sub"/></button>
              <button className="btn btn-xs hover padding-horizontal-1" tabIndex={0}>
                <span className="popup select bottom">
                  <div className="select-container">
                    <div className="sp-item">Удалить</div>
                    <div className="sp-item">Редактировать</div>
                    <div className="sp-item">Пожаловаться</div>
                  </div>
                </span>
                <span className="fa fa-bars text sub"/>
              </button>
            </div>
            <div className="margin-bottom-1">Hello WORLD! Server!</div>
            <div>
              <button className="btn btn-xs text sub padding-horizontal-1 margin-right-d1" tabIndex={0}><span className="fa fa-caret-up margin-right-d1"/>Свернуть</button>
              <button className="btn btn-xs btn-primary padding-horizontal-1" tabIndex={0}><span className="fa fa-reply text margin-right-d1"/>Ответить</button>
            </div>
          
            <div className="sp-tree padding-top-1 margin-left-d2 padding-bottom-d2-ntlast text small">
              <div className="padding-left-d2">
                <div className="margin-bottom-1">
                  <span className="text lite sub margin-left-d1 margin-right-d1 box-xs padding-horizontal-0"><span className="fa fa-caret-up text green margin-right-d1"/>15</span>
                  <button className="btn btn-xs padding-horizontal-1" tabIndex={0}><span className="fa fa-caret-up text green margin-right-d1"/>За</button>
                  <button className="btn btn-xs padding-horizontal-1" tabIndex={0}><span className="fa fa-caret-down text red margin-right-d1"/>Против</button>
                  <div className="sp-tag item hover margin-left-d1">
                    <img className="sp-profile-img-tag" src="/images/avatar-empty.png"/>
                    makame
                    <div className="text subsub margin-left-d1"><span className="fa fa-caret-up text green margin-right-dd1"/>168</div>
                    <span className="bars"/>
                    <span className="popup select bottom">
                      <div className="select-container">
                        <div className="sp-item">Открыть профиль</div>
                        <div className="sp-item">Пожаловаться</div>
                      </div>
                    </span>
                  </div>
                  <span className="text lite sub margin-left-d1 box-xs padding-horizontal-0">17/02/2016</span>
                  <button className="btn btn-xs margin-left-d1 padding-horizontal-1" tabIndex={0}><span className="fa fa-chain text sub"/></button>
                  <button className="btn btn-xs padding-horizontal-1" tabIndex={0}><span className="fa fa-save text sub"/></button>
                  <button className="btn btn-xs hover padding-horizontal-1" tabIndex={0}>
                    <span className="popup select bottom">
                      <div className="select-container">
                        <div className="sp-item">Удалить</div>
                        <div className="sp-item">Редактировать</div>
                        <div className="sp-item">Пожаловаться</div>
                      </div>
                    </span>
                    <span className="fa fa-bars text sub"/>
                  </button>
                </div>
                <div className="margin-bottom-1">Hello WORLD! Server!</div>
                <div>
                  <button className="btn btn-xs text sub padding-horizontal-1 margin-right-d1" tabIndex={0}><span className="fa fa-caret-up margin-right-d1"/>Свернуть</button>
                  <button className="btn btn-xs btn-primary padding-horizontal-1" tabIndex={0}><span className="fa fa-reply text margin-right-d1"/>Ответить</button>
                </div>
          
                <div className="sp-tree padding-top-1 margin-left-d2 padding-bottom-d2-ntlast text small">
                  <div className="padding-left-d2">
                    <div className="margin-bottom-1">
                      <span className="text lite sub margin-left-d1 margin-right-d1 box-xs padding-horizontal-0"><span className="fa fa-caret-up text green margin-right-d1"/>15</span>
                      <button className="btn btn-xs padding-horizontal-1" tabIndex={0}><span className="fa fa-caret-up text green margin-right-d1"/>За</button>
                      <button className="btn btn-xs padding-horizontal-1" tabIndex={0}><span className="fa fa-caret-down text red margin-right-d1"/>Против</button>
                      <div className="sp-tag item hover margin-left-d1">
                        <img className="sp-profile-img-tag" src="/images/avatar-empty.png"/>
                        makame
                        <div className="text subsub margin-left-d1"><span className="fa fa-caret-up text green margin-right-dd1"/>168</div>
                        <span className="bars"/>
                        <span className="popup select bottom">
                          <div className="select-container">
                            <div className="sp-item">Открыть профиль</div>
                            <div className="sp-item">Пожаловаться</div>
                          </div>
                        </span>
                      </div>
                      <span className="text lite sub margin-left-d1 box-xs padding-horizontal-0">17/02/2016</span>
                      <button className="btn btn-xs margin-left-d1 padding-horizontal-1" tabIndex={0}><span className="fa fa-chain text sub"/></button>
                      <button className="btn btn-xs padding-horizontal-1" tabIndex={0}><span className="fa fa-save text sub"/></button>
                      <button className="btn btn-xs hover padding-horizontal-1" tabIndex={0}>
                        <span className="popup select bottom">
                          <div className="select-container">
                            <div className="sp-item">Удалить</div>
                            <div className="sp-item">Редактировать</div>
                            <div className="sp-item">Пожаловаться</div>
                          </div>
                        </span>
                        <span className="fa fa-bars text sub"/>
                      </button>
                    </div>
                    <div className="margin-bottom-1">Hello WORLD! Server!</div>
                    <div>
                      <button className="btn btn-xs text sub padding-horizontal-1 margin-right-d1" tabIndex={0}><span className="fa fa-caret-up margin-right-d1"/>Свернуть</button>
                      <button className="btn btn-xs btn-primary padding-horizontal-1" tabIndex={0}><span className="fa fa-reply text margin-right-d1"/>Ответить</button>
                    </div>

                    <div className="sp-tree padding-top-1 margin-left-d2 padding-bottom-d2-ntlast text small">
                      <div className="padding-left-d2">
                        <div className="margin-bottom-1">
                          <span className="text lite sub margin-left-d1 margin-right-d1 box-xs padding-horizontal-0"><span className="fa fa-caret-up text green margin-right-d1"/>15</span>
                          <button className="btn btn-xs padding-horizontal-1" tabIndex={0}><span className="fa fa-caret-up text green margin-right-d1"/>За</button>
                          <button className="btn btn-xs padding-horizontal-1" tabIndex={0}><span className="fa fa-caret-down text red margin-right-d1"/>Против</button>
                          <div className="sp-tag item hover margin-left-d1">
                            <img className="sp-profile-img-tag" src="/images/avatar-empty.png"/>
                            makame
                            <div className="text subsub margin-left-d1"><span className="fa fa-caret-up text green margin-right-dd1"/>168</div>
                            <span className="bars"/>
                            <span className="popup select bottom">
                              <div className="select-container">
                                <div className="sp-item">Открыть профиль</div>
                                <div className="sp-item">Пожаловаться</div>
                              </div>
                            </span>
                          </div>
                          <span className="text lite sub margin-left-d1 box-xs padding-horizontal-0">17/02/2016</span>
                          <button className="btn btn-xs margin-left-d1 padding-horizontal-1" tabIndex={0}><span className="fa fa-chain text sub"/></button>
                          <button className="btn btn-xs padding-horizontal-1" tabIndex={0}><span className="fa fa-save text sub"/></button>
                          <button className="btn btn-xs hover padding-horizontal-1" tabIndex={0}>
                            <span className="popup select bottom">
                              <div className="select-container">
                                <div className="sp-item">Удалить</div>
                                <div className="sp-item">Редактировать</div>
                                <div className="sp-item">Пожаловаться</div>
                              </div>
                            </span>
                            <span className="fa fa-bars text sub"/>
                          </button>
                        </div>
                        <div className="margin-bottom-1">Hello WORLD! Server!</div>
                        <div>
                          <button className="btn btn-xs text sub padding-horizontal-1 margin-right-d1" tabIndex={0}><span className="fa fa-caret-up margin-right-d1"/>Свернуть</button>
                          <button className="btn btn-xs btn-primary padding-horizontal-1" tabIndex={0}><span className="fa fa-reply text margin-right-d1"/>Ответить</button>
                        </div>

                        <div className="sp-tree padding-top-1 margin-left-d1 padding-bottom-d2-ntlast text small">
                          <div className="padding-left-d2">
                            <div className="margin-bottom-1">
                              <span className="text lite sub margin-left-d1 margin-right-d1 box-xs padding-horizontal-0"><span className="fa fa-caret-up text green margin-right-d1"/>15</span>
                              <button className="btn btn-xs padding-horizontal-1" tabIndex={0}><span className="fa fa-caret-up text green margin-right-d1"/>За</button>
                              <button className="btn btn-xs padding-horizontal-1" tabIndex={0}><span className="fa fa-caret-down text red margin-right-d1"/>Против</button>
                              <div className="sp-tag item hover margin-left-d1">
                                <img className="sp-profile-img-tag" src="/images/avatar-empty.png"/>
                                makame
                                <div className="text subsub margin-left-d1"><span className="fa fa-caret-up text green margin-right-dd1"/>168</div>
                                <span className="bars"/>
                                <span className="popup select bottom">
                                  <div className="select-container">
                                    <div className="sp-item">Открыть профиль</div>
                                    <div className="sp-item">Пожаловаться</div>
                                  </div>
                                </span>
                              </div>
                              <span className="text lite sub margin-left-d1 box-xs padding-horizontal-0">17/02/2016</span>
                              <button className="btn btn-xs margin-left-d1 padding-horizontal-1" tabIndex={0}><span className="fa fa-chain text sub"/></button>
                              <button className="btn btn-xs padding-horizontal-1" tabIndex={0}><span className="fa fa-save text sub"/></button>
                              <button className="btn btn-xs hover padding-horizontal-1" tabIndex={0}>
                                <span className="popup select bottom">
                                  <div className="select-container">
                                    <div className="sp-item">Удалить</div>
                                    <div className="sp-item">Редактировать</div>
                                    <div className="sp-item">Пожаловаться</div>
                                  </div>
                                </span>
                                <span className="fa fa-bars text sub"/>
                              </button>
                            </div>
                            <div className="margin-bottom-1">Hello WORLD! Server!</div>
                            <div>
                              <button className="btn btn-xs btn-primary padding-horizontal-1" tabIndex={0}><span className="fa fa-reply text margin-right-d1"/>Ответить</button>
                            </div>
                          </div>

                          <div className="padding-left-d2">
                            <div className="margin-bottom-1">
                              <span className="text lite sub margin-left-d1 margin-right-d1 box-xs padding-horizontal-0"><span className="fa fa-caret-up text green margin-right-d1"/>15</span>
                              <button className="btn btn-xs padding-horizontal-1" tabIndex={0}><span className="fa fa-caret-up text green margin-right-d1"/>За</button>
                              <button className="btn btn-xs padding-horizontal-1" tabIndex={0}><span className="fa fa-caret-down text red margin-right-d1"/>Против</button>
                              <div className="sp-tag item hover margin-left-d1">
                                <img className="sp-profile-img-tag" src="/images/avatar-empty.png"/>
                                makame
                                <div className="text subsub margin-left-d1"><span className="fa fa-caret-up text green margin-right-dd1"/>168</div>
                                <span className="bars"/>
                                <span className="popup select bottom">
                                  <div className="select-container">
                                    <div className="sp-item">Открыть профиль</div>
                                    <div className="sp-item">Пожаловаться</div>
                                  </div>
                                </span>
                              </div>
                              <span className="text lite sub margin-left-d1 box-xs padding-horizontal-0">17/02/2016</span>
                              <button className="btn btn-xs margin-left-d1 padding-horizontal-1" tabIndex={0}><span className="fa fa-chain text sub"/></button>
                              <button className="btn btn-xs padding-horizontal-1" tabIndex={0}><span className="fa fa-save text sub"/></button>
                              <button className="btn btn-xs hover padding-horizontal-1" tabIndex={0}>
                                <span className="popup select bottom">
                                  <div className="select-container">
                                    <div className="sp-item">Удалить</div>
                                    <div className="sp-item">Редактировать</div>
                                    <div className="sp-item">Пожаловаться</div>
                                  </div>
                                </span>
                                <span className="fa fa-bars text sub"/>
                              </button>
                            </div>
                            <div className="margin-bottom-1">Hello WORLD! Server!</div>
                            <div>
                              <button className="btn btn-xs text sub padding-horizontal-1 margin-right-d1" tabIndex={0}><span className="fa fa-caret-up margin-right-d1"/>Свернуть</button>
                              <button className="btn btn-xs btn-primary padding-horizontal-1" tabIndex={0}><span className="fa fa-reply text margin-right-d1"/>Ответить</button>
                            </div>

                            <div className="sp-tree padding-top-1 margin-left-d1 padding-bottom-d2-ntlast text small">
                              <div className="padding-left-d2">
                                <div className="margin-bottom-1">
                                  <span className="text lite sub margin-left-d1 margin-right-d1 box-xs padding-horizontal-0"><span className="fa fa-caret-up text green margin-right-d1"/>15</span>
                                  <button className="btn btn-xs padding-horizontal-1" tabIndex={0}><span className="fa fa-caret-up text green margin-right-d1"/>За</button>
                                  <button className="btn btn-xs padding-horizontal-1" tabIndex={0}><span className="fa fa-caret-down text red margin-right-d1"/>Против</button>
                                  <div className="sp-tag item hover margin-left-d1">
                                    <img className="sp-profile-img-tag" src="/images/avatar-empty.png"/>
                                    makame
                                    <div className="text subsub margin-left-d1"><span className="fa fa-caret-up text green margin-right-dd1"/>168</div>
                                    <span className="bars"/>
                                    <span className="popup select bottom">
                                      <div className="select-container">
                                        <div className="sp-item">Открыть профиль</div>
                                        <div className="sp-item">Пожаловаться</div>
                                      </div>
                                    </span>
                                  </div>
                                  <span className="text lite sub margin-left-d1 box-xs padding-horizontal-0">17/02/2016</span>
                                  <button className="btn btn-xs margin-left-d1 padding-horizontal-1" tabIndex={0}><span className="fa fa-chain text sub"/></button>
                                  <button className="btn btn-xs padding-horizontal-1" tabIndex={0}><span className="fa fa-save text sub"/></button>
                                  <button className="btn btn-xs hover padding-horizontal-1" tabIndex={0}>
                                    <span className="popup select bottom">
                                      <div className="select-container">
                                        <div className="sp-item">Удалить</div>
                                        <div className="sp-item">Редактировать</div>
                                        <div className="sp-item">Пожаловаться</div>
                                      </div>
                                    </span>
                                    <span className="fa fa-bars text sub"/>
                                  </button>
                                </div>
                                <div className="margin-bottom-1">Hello WORLD! Server!</div>
                                <div>
                                  <button className="btn btn-xs text sub padding-horizontal-1 margin-right-d1" tabIndex={0}><span className="fa fa-caret-up margin-right-d1"/>Свернуть</button>
                                  <button className="btn btn-xs btn-primary padding-horizontal-1" tabIndex={0}><span className="fa fa-reply text margin-right-d1"/>Ответить</button>
                                </div>

                                <div className="sp-tree padding-top-1 margin-left-d1 padding-bottom-d2-ntlast text small">
                                  <div className="padding-left-d2">
                                    <div className="margin-bottom-1">
                                      <span className="text lite sub margin-left-d1 margin-right-d1 box-xs padding-horizontal-0"><span className="fa fa-caret-up text green margin-right-d1"/>15</span>
                                      <button className="btn btn-xs padding-horizontal-1" tabIndex={0}><span className="fa fa-caret-up text green margin-right-d1"/>За</button>
                                      <button className="btn btn-xs padding-horizontal-1" tabIndex={0}><span className="fa fa-caret-down text red margin-right-d1"/>Против</button>
                                      <div className="sp-tag item hover margin-left-d1">
                                        <img className="sp-profile-img-tag" src="/images/avatar-empty.png"/>
                                        makame
                                        <div className="text subsub margin-left-d1"><span className="fa fa-caret-up text green margin-right-dd1"/>168</div>
                                        <span className="bars"/>
                                        <span className="popup select bottom">
                                          <div className="select-container">
                                            <div className="sp-item">Открыть профиль</div>
                                            <div className="sp-item">Пожаловаться</div>
                                          </div>
                                        </span>
                                      </div>
                                      <span className="text lite sub margin-left-d1 box-xs padding-horizontal-0">17/02/2016</span>
                                      <button className="btn btn-xs margin-left-d1 padding-horizontal-1" tabIndex={0}><span className="fa fa-chain text sub"/></button>
                                      <button className="btn btn-xs padding-horizontal-1" tabIndex={0}><span className="fa fa-save text sub"/></button>
                                      <button className="btn btn-xs hover padding-horizontal-1" tabIndex={0}>
                                        <span className="popup select bottom">
                                          <div className="select-container">
                                            <div className="sp-item">Удалить</div>
                                            <div className="sp-item">Редактировать</div>
                                            <div className="sp-item">Пожаловаться</div>
                                          </div>
                                        </span>
                                        <span className="fa fa-bars text sub"/>
                                      </button>
                                    </div>
                                    <div className="margin-bottom-1">Hello WORLD! Server!</div>
                                    <div>
                                      <button className="btn btn-xs text sub padding-horizontal-1 margin-right-d1" tabIndex={0}><span className="fa fa-caret-up margin-right-d1"/>Свернуть</button>
                                      <button className="btn btn-xs btn-primary padding-horizontal-1" tabIndex={0}><span className="fa fa-reply text margin-right-d1"/>Ответить</button>
                                    </div>

                                    <div className="sp-tree padding-top-1 margin-left-d1 padding-bottom-d2-ntlast text small">
                                      <div className="padding-left-d2">
                                        <div className="margin-bottom-1">
                                          <span className="text lite sub margin-left-d1 margin-right-d1 box-xs padding-horizontal-0"><span className="fa fa-caret-up text green margin-right-d1"/>15</span>
                                          <button className="btn btn-xs padding-horizontal-1" tabIndex={0}><span className="fa fa-caret-up text green margin-right-d1"/>За</button>
                                          <button className="btn btn-xs padding-horizontal-1" tabIndex={0}><span className="fa fa-caret-down text red margin-right-d1"/>Против</button>
                                          <div className="sp-tag item hover margin-left-d1">
                                            <img className="sp-profile-img-tag" src="/images/avatar-empty.png"/>
                                            makame
                                            <div className="text subsub margin-left-d1"><span className="fa fa-caret-up text green margin-right-dd1"/>168</div>
                                            <span className="bars"/>
                                            <span className="popup select bottom">
                                              <div className="select-container">
                                                <div className="sp-item">Открыть профиль</div>
                                                <div className="sp-item">Пожаловаться</div>
                                              </div>
                                            </span>
                                          </div>
                                          <span className="text lite sub margin-left-d1 box-xs padding-horizontal-0">17/02/2016</span>
                                          <button className="btn btn-xs margin-left-d1 padding-horizontal-1" tabIndex={0}><span className="fa fa-chain text sub"/></button>
                                          <button className="btn btn-xs padding-horizontal-1" tabIndex={0}><span className="fa fa-save text sub"/></button>
                                          <button className="btn btn-xs hover padding-horizontal-1" tabIndex={0}>
                                            <span className="popup select bottom">
                                              <div className="select-container">
                                                <div className="sp-item">Удалить</div>
                                                <div className="sp-item">Редактировать</div>
                                                <div className="sp-item">Пожаловаться</div>
                                              </div>
                                            </span>
                                            <span className="fa fa-bars text sub"/>
                                          </button>
                                        </div>
                                        <div className="margin-bottom-1">Hello WORLD! Server!</div>
                                        <div>
                                          <button className="btn btn-xs text sub padding-horizontal-1 margin-right-d1" tabIndex={0}><span className="fa fa-caret-up margin-right-d1"/>Свернуть</button>
                                          <button className="btn btn-xs btn-primary padding-horizontal-1" tabIndex={0}><span className="fa fa-reply text margin-right-d1"/>Ответить</button>
                                        </div>

                                        <div className="sp-tree padding-top-1 margin-left-d1 padding-bottom-d2-ntlast text small">
                                          <div className="padding-left-d1">
                                            <div className="margin-bottom-1">
                                              <span className="text lite sub margin-left-d1 margin-right-d1 box-xs padding-horizontal-0"><span className="fa fa-caret-up text green margin-right-d1"/>15</span>
                                              <button className="btn btn-xs padding-horizontal-1" tabIndex={0}><span className="fa fa-caret-up text green margin-right-d1"/>За</button>
                                              <button className="btn btn-xs padding-horizontal-1" tabIndex={0}><span className="fa fa-caret-down text red margin-right-d1"/>Против</button>
                                              <div className="sp-tag item hover margin-left-d1">
                                                <img className="sp-profile-img-tag" src="/images/avatar-empty.png"/>
                                                makame
                                                <div className="text subsub margin-left-d1"><span className="fa fa-caret-up text green margin-right-dd1"/>168</div>
                                                <span className="bars"/>
                                                <span className="popup select bottom">
                                                  <div className="select-container">
                                                    <div className="sp-item">Открыть профиль</div>
                                                    <div className="sp-item">Пожаловаться</div>
                                                  </div>
                                                </span>
                                              </div>
                                              <span className="text lite sub margin-left-d1 box-xs padding-horizontal-0">17/02/2016</span>
                                              <button className="btn btn-xs margin-left-d1 padding-horizontal-1" tabIndex={0}><span className="fa fa-chain text sub"/></button>
                                              <button className="btn btn-xs padding-horizontal-1" tabIndex={0}><span className="fa fa-save text sub"/></button>
                                              <button className="btn btn-xs hover padding-horizontal-1" tabIndex={0}>
                                                <span className="popup select bottom">
                                                  <div className="select-container">
                                                    <div className="sp-item">Удалить</div>
                                                    <div className="sp-item">Редактировать</div>
                                                    <div className="sp-item">Пожаловаться</div>
                                                  </div>
                                                </span>
                                                <span className="fa fa-bars text sub"/>
                                              </button>
                                            </div>
                                            <div className="margin-bottom-1">Hello WORLD! Server!</div>
                                            <div>
                                              <button className="btn btn-xs btn-primary padding-horizontal-1" tabIndex={0}><span className="fa fa-reply text margin-right-d1"/>Ответить</button>
                                            </div>
                                          </div>

                                          <div className="sp-tree padding-top-1 margin-left-px1 padding-bottom-d2-ntlast text small">
                                            <div className="padding-left-d1">
                                              <div className="margin-bottom-1">
                                                <span className="text lite sub margin-left-d1 margin-right-d1 box-xs padding-horizontal-0"><span className="fa fa-caret-up text green margin-right-d1"/>15</span>
                                                <button className="btn btn-xs padding-horizontal-1" tabIndex={0}><span className="fa fa-caret-up text green margin-right-d1"/>За</button>
                                                <button className="btn btn-xs padding-horizontal-1" tabIndex={0}><span className="fa fa-caret-down text red margin-right-d1"/>Против</button>
                                                <div className="sp-tag item hover margin-left-d1">
                                                  <img className="sp-profile-img-tag" src="/images/avatar-empty.png"/>
                                                  makame
                                                  <div className="text subsub margin-left-d1"><span className="fa fa-caret-up text green margin-right-dd1"/>168</div>
                                                  <span className="bars"/>
                                                  <span className="popup select bottom">
                                                    <div className="select-container">
                                                      <div className="sp-item">Открыть профиль</div>
                                                      <div className="sp-item">Пожаловаться</div>
                                                    </div>
                                                  </span>
                                                </div>
                                                <span className="text lite sub margin-left-d1 box-xs padding-horizontal-0">17/02/2016</span>
                                                <button className="btn btn-xs margin-left-d1 padding-horizontal-1" tabIndex={0}><span className="fa fa-chain text sub"/></button>
                                                <button className="btn btn-xs padding-horizontal-1" tabIndex={0}><span className="fa fa-save text sub"/></button>
                                                <button className="btn btn-xs hover padding-horizontal-1" tabIndex={0}>
                                                  <span className="popup select bottom">
                                                    <div className="select-container">
                                                      <div className="sp-item">Удалить</div>
                                                      <div className="sp-item">Редактировать</div>
                                                      <div className="sp-item">Пожаловаться</div>
                                                    </div>
                                                  </span>
                                                  <span className="fa fa-bars text sub"/>
                                                </button>
                                              </div>
                                              <div className="margin-bottom-1">Hello WORLD! Server!</div>
                                              <div>
                                                <button className="btn btn-xs btn-primary padding-horizontal-1" tabIndex={0}><span className="fa fa-reply text margin-right-d1"/>Ответить</button>
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        {/*<div className="message"><div className="loader"></div></div>*/}
      </div>
    </div>
  }
}

render(React.createElement(Application, { }), document.getElementById("app-container"));

// Import Application
// import { Application } from './Core';

// Model contains all application logic
// import { application } from './Model/App';

// Render Application
// render(React.createElement(Application, { application }), document.getElementById("app-container"));

// Removing global initialisation loading
// if (window) {
//   $(window).load(function() {
//     $('body #loading').children().velocity('fadeOut', {duration: 100, complete: function(){$('body #loading').velocity('fadeOut', {duration: 500, complete: function(){$('body #loading').remove()}});}});
//   });
// }