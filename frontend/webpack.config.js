/// <binding />
var ExtractTextPlugin = require("extract-text-webpack-plugin");
var EncodingPlugin = require('webpack-encoding-plugin');

var webpack = require("webpack");

var lessLoader = ExtractTextPlugin.extract(
    "css?sourceMap!less?sourceMap"
);

module.exports = {
  entry: {
        js: './scripts/App.tsx',
        css: "./styles/index.less"
    },
    output: {
        path: '../public',
        filename: "index.[name]"
    },
    resolve: {
        // Add `.ts` and `.tsx` as a resolvable extension.
        extensions: ['', '.webpack.js', '.web.js', '.ts', '.tsx', '.js', '.less', '.css', '.less', '.scss', '.sass']
    },
    module: {
        loaders: [
            { test: /\.(tsx|ts)?$/, loader: 'babel-loader!ts-loader' },
            {
                test: /\.less$/,
                loader: lessLoader
            },
            {
                test: /\.(gif|png|jpg|jpeg|svg)($|\?)/,
                loaders: ['file?&name=fonts/[hash].[ext]']
            },
            {
                test: /\.(woff|woff2|eot|ttf)($|\?)/,
                loaders: ['file?&name=fonts/[hash].[ext]']
            },
        ]
    },
    devtool: 'inline-source-map',
    plugins: [
        new ExtractTextPlugin("index.css", { allChunks: true }),
        new webpack.ProvidePlugin({
            $: "jquery",
            jQuery: "jquery",
            "window.jQuery": "jquery"
        }),
        // new EncodingPlugin({
        //     encoding: 'iso-8859-1'
        // })
    ],
}