var React = require('react');
var Router = require('react-router');
var routes = require('./routes');
var dnode = require('dnode');
var express = require('express');
var app = express();
var gutil = require('gulp-util');

regularPort = 3000;
dnodePort = 3001;

app.get('*', function(req, res) {
  var router = Router.create({
    location: req.url,
    routes: routes
  });
  return router.run(function(Handler, state) {
    gutil.log("Serving to browser via " + regularPort);
    var component = state.routes[1].handler;
    return component.fetchData(state.params, function(err, data) {
      var reactOutput = React.renderToString(React.createElement(Handler, {
        data: data,
        params: state.params
      }));
      return res.send(getHtml(reactOutput, data, state));
    });
  });
});

app.use(express["static"](__dirname + '/public'));

server = app.listen(regularPort, function() {
  return gutil.log("HTTP: Listening on port " + regularPort);
});

getHtml = function(reactOutput, data, state) {
  var response;
  response = '<link rel="stylesheet" href="/index.css" media="screen" charset="iso-8859-1">';
  response += "<script>window.reactData = " + (JSON.stringify(data)) + "</script>";
  response += "<script>window.initialPage = " + state.params.page + "</script>";
  response += '<div id="app" class="container">';
  response += reactOutput;
  response += '</div>';
  return response += "<script src='http://localhost:" + regularPort + "/index.js'></script>";
};